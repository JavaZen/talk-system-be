package ru.javazen.talk.repository;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.javazen.talk.model.Group;
import ru.javazen.talk.model.Role;
import ru.javazen.talk.model.User;

import java.util.Arrays;
import java.util.HashSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { RepositoryTestConfiguration.class })
@Transactional
public class RoleRepositoryTest {

    @Autowired
    private RoleRepository roleRepository;

    @Before
    public void setUp() {
        Role role1 = new Role();
        role1.setAuthority("ROLE_1");

        Role role2 = new Role();
        role2.setAuthority("ROLE_2");

        roleRepository.save(Arrays.asList(role1, role2));
    }

    @Test
    public void testCount() {
        assertEquals(2, roleRepository.count());
    }

    @Test
    public void testFindAll() {
        Iterable<Role> roles = roleRepository.findAll();
        int actualCount = 0;
        for (Role role: roles) {
            actualCount++;
        }
        assertEquals(2, actualCount);
    }

    @Test
    public void testFindByName() {
        Role role = roleRepository.findByAuthority("ROLE_1");
        assertNotNull(role);
    }

    @After
    public void clean() {
        roleRepository.deleteAll();
    }
}
