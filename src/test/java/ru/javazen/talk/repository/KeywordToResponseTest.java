package ru.javazen.talk.repository;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.javazen.talk.model.Keyword;
import ru.javazen.talk.model.KeywordToKeyword;
import ru.javazen.talk.model.KeywordToResponse;
import ru.javazen.talk.model.Response;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { RepositoryTestConfiguration.class })
@Transactional
public class KeywordToResponseTest {

    @Autowired
    private KeywordToResponseRepository keywordToResponseRepository;

    @Autowired
    private KeywordRepository keywordRepository;

    @Autowired
    private ResponseRepository responseRepository;

    private Response parentResponse;
    @Before
    public void setUp() {
        Response parent = new Response();
        parent.setText("parent");

        Keyword child1 = new Keyword();
        Keyword child2 = new Keyword();
        Keyword child3 = new Keyword();

        child1.setText("child 1");
        child2.setText("child 2");
        child3.setText("child 3");

        keywordRepository.save(Arrays.asList(child1, child2, child3));
        responseRepository.save(parent);

        parentResponse = parent;

        KeywordToResponse keywordToResponse1 = new KeywordToResponse();
        keywordToResponse1.setResponse(parent);
        keywordToResponse1.setKeyword(child1);

        KeywordToResponse keywordToResponse2 = new KeywordToResponse();
        keywordToResponse2.setResponse(parent);
        keywordToResponse2.setKeyword(child2);

        KeywordToResponse keywordToResponse3 = new KeywordToResponse();
        keywordToResponse3.setResponse(parent);
        keywordToResponse3.setKeyword(child3);

        keywordToResponseRepository.save(Arrays.asList(keywordToResponse1, keywordToResponse2, keywordToResponse3));
    }

    @Test
    public void testSearchByParen() {
        List<Keyword> keywords = keywordToResponseRepository.getKeywordsByResponseId(parentResponse.getId());

        assertEquals(3, keywords.size());
    }

    @After
    public void clean() {
        keywordToResponseRepository.deleteAll();
        responseRepository.deleteAll();
        keywordRepository.deleteAll();
    }
}
