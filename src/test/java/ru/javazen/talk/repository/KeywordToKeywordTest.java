package ru.javazen.talk.repository;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.javazen.talk.model.Keyword;
import ru.javazen.talk.model.KeywordToKeyword;
import ru.javazen.talk.model.Role;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { RepositoryTestConfiguration.class })
@Transactional
public class KeywordToKeywordTest {

    @Autowired
    private KeywordToKeywordRepository keywordToKeywordRepository;

    @Autowired
    private KeywordRepository keywordRepository;

    private Keyword parentKeyword;
    @Before
    public void setUp() {
        Keyword parent = new Keyword();
        parent.setText("parent");

        Keyword child1 = new Keyword();
        Keyword child2 = new Keyword();
        Keyword child3 = new Keyword();

        child1.setText("child 1");
        child2.setText("child 2");
        child3.setText("child 3");

        keywordRepository.save(Arrays.asList(parent, child1, child2, child3));

        parentKeyword = parent;

        KeywordToKeyword keywordToKeyword1 = new KeywordToKeyword();
        keywordToKeyword1.setKeywordFrom(parent);
        keywordToKeyword1.setKeywordTo(child1);

        KeywordToKeyword keywordToKeyword2 = new KeywordToKeyword();
        keywordToKeyword2.setKeywordFrom(parent);
        keywordToKeyword2.setKeywordTo(child2);

        KeywordToKeyword keywordToKeyword3 = new KeywordToKeyword();
        keywordToKeyword3.setKeywordFrom(parent);
        keywordToKeyword3.setKeywordTo(child3);

        keywordToKeywordRepository.save(Arrays.asList(keywordToKeyword1, keywordToKeyword2, keywordToKeyword3));
    }

    @Test
    public void testSearchByParen() {
        List<Keyword> keywords = keywordToKeywordRepository.getKeywordsByParentId(parentKeyword.getId());

        assertEquals(3, keywords.size());
    }

    @After
    public void clean() {
        keywordToKeywordRepository.deleteAll();
        keywordRepository.deleteAll();
    }
}
