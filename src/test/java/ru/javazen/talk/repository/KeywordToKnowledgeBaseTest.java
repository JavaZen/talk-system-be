package ru.javazen.talk.repository;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.javazen.talk.model.*;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { RepositoryTestConfiguration.class })
@Transactional
public class KeywordToKnowledgeBaseTest {

    @Autowired
    private KeywordRepository keywordRepository;

    @Autowired
    private KnowledgeBaseRepository knowledgeBaseRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private GroupRepository groupRepository;

    private Long knowledgeBaseId;

    private Keyword parentKeyword;
    @Before
    public void setUp() {
        KnowledgeBase knowledgeBase = new KnowledgeBase();
        knowledgeBase.setName("KB Id");

        Group group = new Group();
        group.setName("group");
        groupRepository.save(group);

        User user = new User();
        user.setUsername("user");
        user.setGroup(group);
        user = userRepository.save(user);

        knowledgeBase.setAuthor(user);
        knowledgeBase.setUser(user);

        knowledgeBase = knowledgeBaseRepository.save(knowledgeBase);
        knowledgeBaseId = knowledgeBase.getId();

        Keyword keyword = new Keyword();
        keyword.setText("keyword");
        keyword.setKnowledgeBase(knowledgeBase);

        keywordRepository.save(keyword);
    }

    @Test
    public void testSearchByKnowledgeBase() {
        List<Keyword> keywords = keywordRepository.getKeywordsByKnowledgeBaseId(knowledgeBaseId);

        assertEquals(1, keywords.size());
    }

    @After
    public void clean() {
        keywordRepository.deleteAll();
        knowledgeBaseRepository.deleteAll();
    }
}
