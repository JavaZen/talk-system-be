package ru.javazen.talk.repository;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;
import ru.javazen.talk.model.Group;
import ru.javazen.talk.model.Role;
import ru.javazen.talk.model.User;

import java.util.Arrays;
import java.util.HashSet;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { RepositoryTestConfiguration.class })
@Transactional
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Before
    public void setUp() {

        Group group = new Group();
        group.setName("GROUP");
        groupRepository.save(group);

        Role role = new Role();
        role.setAuthority("ROLE");
        roleRepository.save(role);

        User user1 = new User();
        user1.setUsername("user1");
        user1.setGroup(group);
        user1.setRoles(new HashSet<>(Arrays.asList(role)));

        User user2 = new User();
        user2.setUsername("user2");
        user2.setGroup(group);

        userRepository.save(Arrays.asList(user1, user2));
    }

    @Test
    public void testCount() {
        assertEquals(2, userRepository.count());
    }

    @Test
    public void testFindAll() {
        Iterable<User> users = userRepository.findAll();
        int actualCount = 0;
        for (User user: users) {
            actualCount++;
        }
        assertEquals(2, actualCount);
    }

    @Test
    public void testFindByName() {
        User user = userRepository.findByUsernameIgnoreCase("user1");
        assertNotNull(user);
    }

    @After
    public void clean() {
        userRepository.deleteAll();
        groupRepository.deleteAll();
        roleRepository.deleteAll();
    }
}
