package ru.javazen.talk.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource("/WEB-INF/service.properties")
@Import({ RestConfig.class, RepositoryConfiguration.class, AppConfig.class, SecurityConfiguration.class })
@ComponentScan("ru.javazen.talk.service.impl")
public class RootConfig {  }
