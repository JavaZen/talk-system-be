package ru.javazen.talk.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.support.ConversionServiceFactoryBean;
import org.springframework.core.convert.converter.Converter;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import ru.javazen.talk.rest.transport.request.converter.ResponseRequestConverter;
import ru.javazen.talk.rest.transport.response.converter.ChatResponseConverter;
import ru.javazen.talk.rest.transport.response.converter.GroupResponseConverter;
import ru.javazen.talk.rest.transport.response.converter.KeywordAsTreeResponseConverter;
import ru.javazen.talk.rest.transport.response.converter.UserResponseConverter;

import java.util.HashSet;
import java.util.Set;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages = {"ru.javazen.talk.rest"})
@Import({ RepositoryConfiguration.class, SecurityConfiguration.class })
public class RestConfig extends WebMvcConfigurerAdapter {

    @Autowired
    private Environment environment;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**").allowedOrigins(environment.getProperty("client.web.url"), "*");
    }

    @Bean
    public ConversionServiceFactoryBean conversionService() {
        ConversionServiceFactoryBean conversionServiceFactoryBean = new ConversionServiceFactoryBean();

        Set<Converter> converters = new HashSet<>();
        converters.add(new GroupResponseConverter());
        converters.add(new UserResponseConverter());
        converters.add(new KeywordAsTreeResponseConverter());
        converters.add(new ChatResponseConverter());
        converters.add(new ResponseRequestConverter());

        conversionServiceFactoryBean.setConverters(converters);

        return conversionServiceFactoryBean;
    }
}