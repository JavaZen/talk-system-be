package ru.javazen.talk.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.annotation.EnableAsync;
import ru.javazen.talk.analysis.grapheme.GraphemeAnalyzer;
import ru.javazen.talk.analysis.grapheme.impl.OpenNlpGraphemeAnalyzer;
import ru.javazen.talk.analysis.morphology.MorphologyAnalyzer;
import ru.javazen.talk.analysis.morphology.impl.JLanguageToolMorphologyAnalyzer;
import ru.javazen.talk.analysis.morphology.impl.OpenNlpMorphologyAnalyzer;
import ru.javazen.talk.analysis.util.KeywordComparator;
import ru.javazen.talk.analysis.util.Lemmatizer;
import ru.javazen.talk.engine.DbKnowledgeBaseRepository;
import ru.javazen.talk.knowledgebase.repository.KnowledgeBaseRepository;
import ru.javazen.talk.service.DialogueSystemService;
import ru.javazen.talk.service.impl.DialogueSystemServiceImpl;

import java.io.IOException;

@EnableAsync
@Configuration
public class AppConfig {

    @Bean
    public DialogueSystemService dialogueSystemService() {

        return new DialogueSystemServiceImpl() {
            @Override
            protected KnowledgeBaseRepository getKnowledgeBaseRepository(Long kbId) {
                DbKnowledgeBaseRepository repository = dbKnowledgeBaseRepository();
                repository.setKnowledgeBaseId(kbId);

                return repository;
            }
        };
    }

    @Bean
    @Scope("prototype")
    public DbKnowledgeBaseRepository dbKnowledgeBaseRepository() {
        return new DbKnowledgeBaseRepository();
    }

    @Bean
    public GraphemeAnalyzer graphemeAnalyzer() throws IOException {
        GraphemeAnalyzer analyzer = new OpenNlpGraphemeAnalyzer(OpenNlpGraphemeAnalyzer.getDefaultModelRu());
        return analyzer;
    }

    @Bean
    public MorphologyAnalyzer morphologyAnalyzer() throws IOException {
        MorphologyAnalyzer analyzer = new OpenNlpMorphologyAnalyzer(OpenNlpMorphologyAnalyzer.getDefaultModelRu());
        return analyzer;
    }

    @Bean
    public JLanguageToolMorphologyAnalyzer jLanguageToolMorphologyAnalyzer() {
        return new JLanguageToolMorphologyAnalyzer();
    }

    @Bean
    public OpenNlpMorphologyAnalyzer openNlpMorphologyAnalyzer() throws IOException {
        return new OpenNlpMorphologyAnalyzer(OpenNlpMorphologyAnalyzer.getDefaultModelRu());
    }

    @Bean
    public Lemmatizer lemmatizer() throws IOException {
        Lemmatizer lemmatizer = new Lemmatizer();
        lemmatizer.setjLanguageToolMorphologyAnalyzer(jLanguageToolMorphologyAnalyzer());
        lemmatizer.setOpenNlpMorphologyAnalyzer(openNlpMorphologyAnalyzer());
        return lemmatizer;
    }

    @Bean
    public KeywordComparator keywordComparator() throws IOException {
        KeywordComparator comparator = new KeywordComparator();
        comparator.setLemmatizer(lemmatizer());
        return comparator;
    }
}
