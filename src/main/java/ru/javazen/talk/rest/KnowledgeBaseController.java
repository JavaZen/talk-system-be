package ru.javazen.talk.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import ru.javazen.talk.engine.DbKnowledgeBaseRepository;
import ru.javazen.talk.knowledgebase.Keyword;
import ru.javazen.talk.knowledgebase.KeywordToKeyword;
import ru.javazen.talk.knowledgebase.KeywordToResponse;
import ru.javazen.talk.knowledgebase.Response;
import ru.javazen.talk.knowledgebase.ResponseToResponse;
import ru.javazen.talk.model.*;
import ru.javazen.talk.model.KnowledgeBase;
import ru.javazen.talk.repository.KnowledgeBaseRepository;
import ru.javazen.talk.repository.UserRepository;
import ru.javazen.talk.rest.status.ResourceNotFoundException;
import ru.javazen.talk.rest.transport.request.KnowledgeBaseRequest;
import ru.javazen.talk.rest.transport.response.KnowledgeBaseResponse;
import ru.javazen.talk.service.UserService;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("knowledge_base")
public class KnowledgeBaseController {

    @Autowired
    private KnowledgeBaseRepository knowledgeBaseRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private DbKnowledgeBaseRepository dbKnowledgeBaseRepository;

    @RequestMapping(path = "/{id}/get")
    public KnowledgeBaseResponse getById(@PathVariable Long id) {
        KnowledgeBase knowledgeBase = knowledgeBaseRepository.findOne(id);
        if (knowledgeBase == null) {
            throw new ResourceNotFoundException();
        }

        KnowledgeBaseResponse response = convertToTransport(knowledgeBase);

        return response;
    }

    @RequestMapping(path = "/get/all")
    public List<KnowledgeBaseResponse> getAll() {
        Iterable<KnowledgeBase> knowledgeBases = knowledgeBaseRepository.findAll();

        List<KnowledgeBaseResponse> responses = new ArrayList<>();
        for (KnowledgeBase knowledgeBase : knowledgeBases) {
            KnowledgeBaseResponse response = convertToTransport(knowledgeBase);
            responses.add(response);
        }
        return responses;
    }

    @Transactional
    @RequestMapping(path = "/add", method = RequestMethod.POST)
    public ResponseEntity<Long> add(@RequestBody KnowledgeBaseRequest knowledgeBaseRequest) {
        KnowledgeBase knowledgeBase = new KnowledgeBase();

        knowledgeBase.setName(knowledgeBaseRequest.getName());
        knowledgeBase.setDescription(knowledgeBaseRequest.getDescription());
        knowledgeBase.setCreatedWhen(new Date());
        knowledgeBase.setAuthor(userService.getCurrentUser());

        // create user for KB
        User user = new User();
        user.setUsername(knowledgeBaseRequest.getName());
        user.setPassword(knowledgeBaseRequest.getName() + userService.getCurrentUser().getUsername());
        user = userService.registerKbUser(user);
        knowledgeBase.setUser(user);

        knowledgeBase = knowledgeBaseRepository.save(knowledgeBase);

        return ResponseEntity.ok(knowledgeBase.getId());
    }

    @RequestMapping(path = "/update", method = RequestMethod.POST)
    public void update(@RequestBody KnowledgeBaseRequest knowledgeBaseRequest) {
        KnowledgeBase knowledgeBase = knowledgeBaseRepository.findOne(knowledgeBaseRequest.getId());
        if (knowledgeBase == null) {
            throw new ResourceNotFoundException();
        }
        knowledgeBase.setName(knowledgeBaseRequest.getName());
        knowledgeBase.setAuthor(userRepository.findOne(knowledgeBaseRequest.getUserId()));

        knowledgeBaseRepository.save(knowledgeBase);
    }

    @RequestMapping(path = "/{id}/delete", method = RequestMethod.POST)
    public void delete(@PathVariable Long id) {
        knowledgeBaseRepository.delete(id);
    }

    @RequestMapping(path = "/{id}/download", method = RequestMethod.GET)
    public ResponseEntity download(@PathVariable("id") Long id) {

        dbKnowledgeBaseRepository.setKnowledgeBaseId(id);

        ru.javazen.talk.knowledgebase.KnowledgeBase container = new ru.javazen.talk.knowledgebase.KnowledgeBase();
        Collection<Keyword> keywords = dbKnowledgeBaseRepository.getAllKeywords();
        if (keywords != null) {
            container.setKeywords(keywords.stream().collect(Collectors.toList()));
        }
        Collection<Response> responses = dbKnowledgeBaseRepository.getAllResponse();
        if (responses != null) {
            container.setResponses(responses.stream().collect(Collectors.toList()));
        }
        Collection<ResponseToResponse> responseToResponses = dbKnowledgeBaseRepository.getAllResponseToResponses();
        if (responseToResponses != null) {
            container.setResponseToResponses(responseToResponses.stream().collect(Collectors.toList()));
        }
        Collection<KeywordToKeyword> keywordToKeywords = dbKnowledgeBaseRepository.getAllKeywordToKeywords();
        if (keywordToKeywords != null) {
            container.setKeywordToKeywords(keywordToKeywords.stream().collect(Collectors.toList()));
        }
        Collection<KeywordToResponse> keywordToResponses = dbKnowledgeBaseRepository.getAllKeywordToResponses();
        if (keywordToResponses != null) {
            container.setKeywordToResponses(keywordToResponses.stream().collect(Collectors.toList()));
        }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(ru.javazen.talk.knowledgebase.KnowledgeBase.class);
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            jaxbMarshaller.marshal(container, outputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
        byte[] res = outputStream.toByteArray();
        return ResponseEntity.ok()
                .contentLength(res.length)
                .contentType(MediaType.TEXT_XML)
                .body(res);
    }

    private KnowledgeBaseResponse convertToTransport(KnowledgeBase knowledgeBase) {
        KnowledgeBaseResponse response = new KnowledgeBaseResponse();

        response.setId(knowledgeBase.getId());
        response.setName(knowledgeBase.getName());
        response.setDescription(knowledgeBase.getDescription());
        response.setCreatedWhen(knowledgeBase.getCreatedWhen());

        KnowledgeBaseResponse.SimpleUser simpleUser = new KnowledgeBaseResponse.SimpleUser();
        simpleUser.setId(knowledgeBase.getAuthor().getId());
        simpleUser.setUsername(knowledgeBase.getAuthor().getUsername());
        response.setAuthor(simpleUser);

        response.setUserId(knowledgeBase.getUser().getId());

        return response;
    }
}