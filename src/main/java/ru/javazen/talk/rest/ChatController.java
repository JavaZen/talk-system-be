package ru.javazen.talk.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.javazen.talk.model.*;
import ru.javazen.talk.repository.*;
import ru.javazen.talk.rest.transport.request.ChatRequest;
import ru.javazen.talk.rest.transport.request.ChatUpdateRequest;
import ru.javazen.talk.rest.transport.response.ChatResponse;
import ru.javazen.talk.rest.transport.response.ChatUpdateResponse;
import ru.javazen.talk.service.ChatUpdateService;
import ru.javazen.talk.service.UserService;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping("/chat")
public class ChatController {

    @Autowired
    private ChatUpdateService chatUpdateService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private ChatRepository chatRepository;

    @Autowired
    private ChatParticipantRepository chatParticipantRepository;

    @Autowired
    KnowledgeBaseRepository knowledgeBaseRepository;

    @Autowired
    ConversionService conversionService;

    @RequestMapping(path = "/create", method = RequestMethod.POST)
    public ResponseEntity<ChatResponse> createDialog(@RequestBody ChatRequest chatRequest) {

        Chat chat = new Chat();
        chat.setTitle(chatRequest.getTitle());
        chat.setGroup(chatRequest.getGroup());
        chat = chatRepository.save(chat);

        Iterable<User> usersIterable = userRepository.findAll(chatRequest.getMembers());

        Set<User> users = StreamSupport.stream(usersIterable.spliterator(), false).collect(Collectors.toSet());
        User owner = userService.getCurrentUser();
        if (!users.stream().filter(user -> user.getId().equals(owner.getId())).findFirst().isPresent()) {
            users.add(owner);
        }

        List<ChatParticipant> chatParticipants = new ArrayList<>();

        for (User user : users) {
            ChatParticipant participant = new ChatParticipant();
            participant.setChat(chat);
            participant.setUser(user);
            participant.setParticipantRole(user.equals(owner) ?
                    ChatParticipant.ParticipantRole.OWNER :
                    ChatParticipant.ParticipantRole.MEMBER);

            chatParticipants.add(participant);
        }
        chatParticipantRepository.save(chatParticipants);
        chat = chatRepository.findOne(chat.getId());
        return ResponseEntity.ok(conversionService.convert(chat, ChatResponse.class));
    }

    @RequestMapping(path = "/create/kb{kbId}", method = RequestMethod.POST)
    public ResponseEntity<ChatResponse> createChatByKb(
            @PathVariable("kbId") Long kbId,
            @RequestBody ChatRequest chatRequest) {

        KnowledgeBase base = knowledgeBaseRepository.findOne(kbId);
        List<Long> members = new ArrayList<>();
        members.add(base.getUser().getId());
        members.add(userService.getCurrentUser().getId());

        chatRequest.setMembers(members);
        chatRequest.setGroup(false);
        return createDialog(chatRequest);
    }

    @RequestMapping(method = RequestMethod.POST, path = "/update/send")
    public void sendUpdate(@RequestBody ChatUpdateRequest updateRequest) {
        ChatUpdate update = new ChatUpdate();
        update.setMessage(updateRequest.getMessage());

        Chat chat = chatRepository.findOne(updateRequest.getChatId());
        update.setChat(chat);

        User user = userService.getCurrentUser();
        update.setUser(user);

        update.setSentWhen(new Date());

        chatUpdateService.sendUpdate(update);
    }

    @RequestMapping("/{id}/update/get/last{count}")
    public List<ChatUpdateResponse> getLastUpdates(
            @PathVariable("id") Long chatId,
            @PathVariable("count") Integer lastUpdatesCount) {

        List<ChatUpdate> updates = chatUpdateService.getLastUpdates(chatId, lastUpdatesCount);

        return convertUpdatesToResponses(updates);
    }

    @RequestMapping("/{id}/update/get/count{count}/offset{offset}")
    public List<ChatUpdateResponse> getUpdatesWithOffset(
            @PathVariable("id") Long chatId,
            @PathVariable("count") Integer updatesCount,
            @PathVariable("offset") Long offsetUpdateId) {

        List<ChatUpdate> updates = chatUpdateService.getUpdatesWithOffset(chatId, updatesCount, offsetUpdateId);

        return convertUpdatesToResponses(updates);
    }

    @RequestMapping("/{id}/update/last/from{from}")
    public List<ChatUpdateResponse> getUpdatesFromUpdate(
            @PathVariable("id") Long chatId,
            @PathVariable("from") Long fromUpdateId) {

        List<ChatUpdate> updates = chatUpdateService.getUpdatesFromMessage(chatId, fromUpdateId);

        return convertUpdatesToResponses(updates);
    }

    @RequestMapping("/get/users/first{first}/second{second}")
    public List<ChatResponse> getChatsByUsers(
            @PathVariable("first") Long firstUserId,
            @PathVariable("second") Long secondUserId) {

        List<Chat> chats = chatRepository.findChatByParticipants(firstUserId, secondUserId);
        List<ChatResponse> responses = new ArrayList<>();
        for (Chat chat : chats) {
            responses.add(conversionService.convert(chat, ChatResponse.class));
        }
        return responses;
    }

    @RequestMapping("/get/kb{kbId}")
    public List<ChatResponse> getChatsByKnowledgeBases(@PathVariable("kbId") Long kbId) {
        KnowledgeBase base = knowledgeBaseRepository.findOne(kbId);
        Long firstUserId = userService.getCurrentUser().getId();
        Long secondUserId = base.getUser().getId();

        return getChatsByUsers(firstUserId, secondUserId);
    }

    @RequestMapping("/get/all")
    public List<ChatResponse> getAll() {
        List<Chat> chats = chatRepository.findChatByParticipant(userService.getCurrentUser().getId());
        List<ChatResponse> responses = new ArrayList<>();
        for (Chat chat : chats) {
            responses.add(conversionService.convert(chat, ChatResponse.class));
        }
        return responses;
    }

    @RequestMapping("/{id}/get")
    public ChatResponse getChatById(
            @PathVariable("id") Long chatId) {

        Chat chat = chatRepository.findOne(chatId);

        return conversionService.convert(chat, ChatResponse.class);
    }

    private List<ChatUpdateResponse> convertUpdatesToResponses(List<ChatUpdate> updates) {
        List<ChatUpdateResponse> responses = new ArrayList<>();
        for (ChatUpdate update : updates) {
            ChatUpdateResponse response = new ChatUpdateResponse();
            response.setId(update.getId());
            response.setChatId(update.getChat().getId());
            response.setUserId(update.getUser().getId());
            response.setMessage(update.getMessage());
            response.setSentWhen(update.getSentWhen());

            responses.add(response);
        }

        return responses;
    }

    @RequestMapping(path = "/{id}/reset", method = RequestMethod.POST)
    public void reset(@PathVariable("id") Long id) {
        chatUpdateService.resetContext(id);
    }
}