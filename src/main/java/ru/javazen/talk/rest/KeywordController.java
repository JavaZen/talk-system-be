package ru.javazen.talk.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import ru.javazen.talk.model.Keyword;
import ru.javazen.talk.model.KeywordToKeyword;
import ru.javazen.talk.model.KeywordToResponse;
import ru.javazen.talk.model.Response;
import ru.javazen.talk.repository.*;
import ru.javazen.talk.rest.status.ResourceNotFoundException;
import ru.javazen.talk.rest.transport.request.KeywordRequest;
import ru.javazen.talk.rest.transport.request.KeywordToKeywordRequest;
import ru.javazen.talk.rest.transport.request.KeywordToResponseRequest;
import ru.javazen.talk.rest.transport.response.*;

import java.lang.reflect.Array;
import java.util.*;

@RestController
@RequestMapping("/keyword")
public class KeywordController {

    @Autowired
    private KeywordRepository keywordRepository;

    @Autowired
    private KnowledgeBaseRepository knowledgeBaseRepository;

    @Autowired
    private KeywordToKeywordRepository keywordToKeywordRepository;

    @Autowired
    private KeywordToResponseRepository keywordToResponseRepository;

    @Autowired
    private ResponseRepository responseRepository;

    @Autowired
    private ConversionService conversionService;

    @RequestMapping("/get/list/response{responseId}")
    public List<Keyword> getListByResponseId(@PathVariable Long responseId) {
        List<Keyword> keywords = keywordToResponseRepository.getKeywordsByResponseId(responseId);
        return keywords;
    }

    //TODO !!!!!!!!!!!!!! Use transport model!
    @RequestMapping("/get/list/kb{knowledgeBaseId}")
    public List<Keyword> getListByKnowledgeBaseId(@PathVariable Long knowledgeBaseId) {
        List<Keyword> keywords = keywordRepository.getKeywordsByKnowledgeBaseId(knowledgeBaseId);
        return keywords;
    }

    @RequestMapping("/{id}/get/tree")
    public KeywordAsTreeResponse getTreeByKeywordId(@PathVariable Long id) {
        Keyword keyword = keywordRepository.findOne(id);
        if (keyword == null) {
            throw new ResourceNotFoundException();
        }
        KeywordAsTreeResponse keywordAsTreeResponse = buildTree(keyword);
        return keywordAsTreeResponse;
    }

    @RequestMapping("/get/tree/response{responseId}")
    public List<KeywordAsTreeResponse> getTreeByResponseId(@PathVariable Long responseId) {
        List<Keyword> keywords = keywordToResponseRepository.getKeywordsByResponseId(responseId);

        List<KeywordAsTreeResponse> keywordAsTreeResponses = new ArrayList<>();
        for (Keyword keyword : keywords) {
            KeywordAsTreeResponse keywordAsTreeResponse = buildTree(keyword);
            keywordAsTreeResponses.add(keywordAsTreeResponse);
        }
        return keywordAsTreeResponses;
    }

    @RequestMapping("/get/tree/kb{knowledgeBaseId}")
    public List<RootKeywordForTree> getTreeByKnowledgeBaseId(@PathVariable Long knowledgeBaseId) {
        List<Keyword> keywords = keywordRepository.getKeywordsByKnowledgeBaseId(knowledgeBaseId);

        List<RootKeywordForTree> rootKeywordsForTree = new ArrayList<>();
        for (Keyword keyword : keywords) {
            KeywordAsTreeResponse keywordAsTreeResponse = buildTree(keyword);
            RootKeywordForTree rootKeywordForTree = new RootKeywordForTree();
            rootKeywordForTree.setKeywordAsTreeResponse(keywordAsTreeResponse);
            rootKeywordsForTree.add(rootKeywordForTree);
        }
        return rootKeywordsForTree;
    }

    @RequestMapping("/{id}/get")
    public KeywordResponse getById(@PathVariable Long id) {
        Keyword keyword = keywordRepository.findOne(id);

        KeywordResponse keywordResponse = new KeywordResponse();
        keywordResponse.setText(keyword.getText());

        keywordResponse.setKnowledgeBaseId(keyword.getKnowledgeBase().getId());

        Set<KeywordResponse.Flag> flags = new HashSet<>();
        for (Keyword.Flag flag : keyword.getFlags()) {
            KeywordResponse.Flag flagResponse = new KeywordResponse.Flag();
            flagResponse.setName(flag.getName());
            flagResponse.setValue(flag.getName());
            flags.add(flagResponse);
        }
        keywordResponse.setFlags(flags);
        return keywordResponse;
    }

    @RequestMapping(path = "/add", method = RequestMethod.POST)
    public ResponseEntity<Long> add(@RequestBody KeywordRequest keywordRequest) {
        Keyword keyword = new Keyword();

        keyword.setId(keywordRequest.getId());
        keyword.setText(keywordRequest.getText());

        if (keywordRequest.getFlags() != null) {
            Set<Keyword.Flag> flags = new HashSet<>();
            for (KeywordRequest.Flag flagRequest : keywordRequest.getFlags()) {
                Keyword.Flag flag = new Keyword.Flag();
                flag.setName(flagRequest.getName());
                flag.setValue(flagRequest.getValue());
                flags.add(flag);
            }
            keyword.setFlags(flags);
        }
        if (keywordRequest.getKnowledgeBaseId() != null) {
            keyword.setKnowledgeBase(knowledgeBaseRepository.findOne(keywordRequest.getKnowledgeBaseId()));
        }

        keywordRepository.save(keyword);

        return ResponseEntity.ok(keyword.getId());
    }

    @RequestMapping(path = "/relate/k2k/save", method = RequestMethod.POST)
    public ResponseEntity<Long> addK2KRelation(@RequestBody KeywordToKeywordRequest keywordToKeywordRequest) {
        Keyword keywordFrom = keywordRepository.findOne(keywordToKeywordRequest.getKeywordFromId());
        Keyword keywordTo = keywordRepository.findOne(keywordToKeywordRequest.getKeywordToId());
        if (keywordFrom == null || keywordTo == null) {
            throw new ResourceNotFoundException();
        }

        KeywordToKeyword keywordToKeyword = new KeywordToKeyword();
        keywordToKeyword.setKeywordFrom(keywordFrom);
        keywordToKeyword.setKeywordTo(keywordTo);
        /*keywordToKeyword.setTypeOfRelation(KeywordToKeyword.TypeOfRelation
                .valueOf(keywordToKeywordRequest.getTypeOfRelation().getName()));*/
        //TODO
        keywordToKeyword.setTypeOfRelation(KeywordToKeyword.TypeOfRelation.SYNONYM);

        keywordToKeywordRepository.save(keywordToKeyword);

        return ResponseEntity.ok(keywordToKeyword.getId());
    }

    //todo - implement new patch methodology
    @RequestMapping(path = "/relate/k2k/update", method = RequestMethod.POST)
    public void updateK2KRelation(@RequestBody KeywordToKeywordRequest keywordToKeywordRequest) {

        KeywordToKeyword keywordToKeyword = keywordToKeywordRepository
                .findOne(keywordToKeywordRequest.getRelationId());

        if (keywordToKeyword == null) {
            throw new ResourceNotFoundException();
        }

        /*if (keywordToKeywordRequest.getTypeOfRelation() != null) {
            keywordToKeyword.setTypeOfRelation(KeywordToKeyword.TypeOfRelation
                    .valueOf(keywordToKeywordRequest.getTypeOfRelation().getName()));
        }*/
        //TODO
        keywordToKeyword.setTypeOfRelation(KeywordToKeyword.TypeOfRelation.SYNONYM);
        if (keywordToKeywordRequest.getKeywordFromId() != null) {
            Keyword keywordFrom = keywordRepository.findOne(keywordToKeywordRequest.getKeywordFromId());
            keywordToKeyword.setKeywordFrom(keywordFrom);
        }
        if (keywordToKeywordRequest.getKeywordToId() != null) {
            Keyword keywordTo = keywordRepository.findOne(keywordToKeywordRequest.getKeywordFromId());
            keywordToKeyword.setKeywordFrom(keywordTo);
        }

        keywordToKeywordRepository.save(keywordToKeyword);
    }

    @RequestMapping(path = "/relate/k2r/save", method = RequestMethod.POST)
    public ResponseEntity<Long> addK2RRelation(@RequestBody KeywordToResponseRequest keywordToResponseRequest) {
        Keyword keyword = keywordRepository.findOne(keywordToResponseRequest.getKeywordId());
        Response response = responseRepository.findOne(keywordToResponseRequest.getResponseId());

        if (keyword == null || response == null) {
            throw new ResourceNotFoundException();
        }

        KeywordToResponse keywordToResponse = new KeywordToResponse();
        keywordToResponse.setKeyword(keyword);
        keywordToResponse.setResponse(response);
        keywordToResponse.setTypeOfRelation("none");

        keywordToResponseRepository.save(keywordToResponse);

        return ResponseEntity.ok(keywordToResponse.getId());
    }

    //todo - implement new patch methodology
    @RequestMapping(path = "/relate/k2r/update", method = RequestMethod.POST)
    public void updateK2RRelation(@RequestBody KeywordToResponseRequest keywordToResponseRequest) {

        KeywordToResponse keywordToResponse = keywordToResponseRepository.findOne(keywordToResponseRequest.getRelationId());

        if (keywordToResponse == null) {
            throw new ResourceNotFoundException();
        }

        if (keywordToResponseRequest.getKeywordId() != null) {
            Keyword keyword = keywordRepository.findOne(keywordToResponseRequest.getKeywordId());
            keywordToResponse.setKeyword(keyword);
        }

        if (keywordToResponseRequest.getResponseId() != null) {
            Response response = responseRepository.findOne(keywordToResponseRequest.getResponseId());
            keywordToResponse.setResponse(response);
        }
        if (keywordToResponse.getTypeOfRelation() != null) {
            keywordToResponse.setTypeOfRelation(keywordToResponse.getTypeOfRelation());
        }

        keywordToResponseRepository.save(keywordToResponse);
    }

    @Transactional
    @RequestMapping(path = "/unrelate/keyword{keywordId}/response{responseId}", method = RequestMethod.POST)
    public void unrelateK2R(@PathVariable Long keywordId, @PathVariable Long responseId) {

        keywordToResponseRepository.removeKeywordRelationsByKeywordAndResponseIds(keywordId, responseId);
    }

    @RequestMapping(path = "/update", method = RequestMethod.POST)
    public void update(@RequestBody KeywordRequest keywordRequest) {
        Keyword keyword = keywordRepository.findOne(keywordRequest.getId());
        if (keyword == null) {
            throw new ResourceNotFoundException();
        }
        keyword.setText(keywordRequest.getText());
        if (keywordRequest.getKnowledgeBaseId() != null) {
            keyword.setKnowledgeBase(knowledgeBaseRepository.findOne(keywordRequest.getKnowledgeBaseId()));
        }

        if (keywordRequest.getFlags() != null) {
            Set<Keyword.Flag> flags = new HashSet<>();
            for (KeywordRequest.Flag flagRequest : keywordRequest.getFlags()) {
                Keyword.Flag flag = new Keyword.Flag();
                flag.setName(flagRequest.getName());
                flag.setValue(flagRequest.getValue());
                flags.add(flag);
            }
            keyword.setFlags(flags);
        }

        keywordRepository.save(keyword);
    }

    @Transactional
    @RequestMapping(path = "/{id}/delete", method = RequestMethod.POST)
    public void delete(@PathVariable Long id) {
        Keyword keyword = keywordRepository.findOne(id);
        deleteTree(keyword);
    }

    private void deleteTree(Keyword keyword) {
        List<KeywordToKeyword> k2ks = keywordToKeywordRepository.getRelationsByParentId(keyword.getId());

        for (KeywordToKeyword k2k : k2ks) {
            deleteTree(k2k.getKeywordTo());
            keywordToKeywordRepository.delete(k2k);
        }
        keywordToResponseRepository.removeKeywordRelationsByKeywordId(keyword.getId());

        keywordRepository.delete(keyword);
    }

    @RequestMapping(path = "/flags/get/all", method = RequestMethod.GET)
    public Set<KeywordResponse.Flag> getAllFlags() {
        Set<KeywordResponse.Flag> flags = new HashSet<>();

        KeywordResponse.Flag mask = new KeywordResponse.Flag();
        mask.setValue("маска");
        mask.setName("MASKED");

        KeywordResponse.Flag phrase = new KeywordResponse.Flag();
        phrase.setValue("фраза");
        phrase.setName("PHRASE");

        flags.addAll(Arrays.asList(mask, phrase));
        return flags;
    }

    @RequestMapping(path = "/typeOfRelation/get/all", method = RequestMethod.GET)
    public List<TypeOfRelationBetweenKeywordsResponse> getTypesOfRelation() {
        List<TypeOfRelationBetweenKeywordsResponse> responses = new ArrayList<>();

        for (KeywordToKeyword.TypeOfRelation typeOfRelation : KeywordToKeyword.TypeOfRelation.values()) {
            TypeOfRelationBetweenKeywordsResponse response = new TypeOfRelationBetweenKeywordsResponse();
            response.setName(typeOfRelation.name());
            response.setValue(typeOfRelation.getValue());
            responses.add(response);
        }

        return responses;
    }

    private KeywordAsTreeResponse buildTree(Keyword keyword) {
        KeywordAsTreeResponse keywordAsTreeResponse
                = conversionService.convert(keyword, KeywordAsTreeResponse.class);

        List<KeywordToKeyword> keywordToKeywords = keywordToKeywordRepository.getRelationsByParentId(keyword.getId());

        List<KeywordAsTreeResponse.KeywordRelation> keywordRelations = new ArrayList<>();
        for (KeywordToKeyword keywordToKeyword : keywordToKeywords) {
            KeywordAsTreeResponse.KeywordRelation keywordRelation = new KeywordAsTreeResponse.KeywordRelation();
            keywordRelation.setRelationId(keywordToKeyword.getId());


            TypeOfRelationBetweenKeywordsResponse tk2kResponse = new TypeOfRelationBetweenKeywordsResponse();
            tk2kResponse.setName(keywordToKeyword.getTypeOfRelation().name());
            tk2kResponse.setValue(keywordToKeyword.getTypeOfRelation().getValue());
            keywordRelation.setTypeOfRelation(tk2kResponse);
            keywordRelation.setKeywordAsTreeResponse(buildTree(keywordToKeyword.getKeywordTo()));

            keywordRelations.add(keywordRelation);
        }
        keywordAsTreeResponse.setKeywordRelations(keywordRelations);

        return keywordAsTreeResponse;
    }
}