package ru.javazen.talk.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.async.DeferredResult;
import ru.javazen.talk.rest.transport.response.LongPollingUpdate;
import ru.javazen.talk.service.LongPollingService;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/longpolling")
public class LongPollingController {

    @Autowired
    private LongPollingService longPollingService;

    @RequestMapping(method = RequestMethod.GET)
    public DeferredResult<List<LongPollingUpdate>> longpolling(
            @RequestParam(name ="type", required = false) String type,
            @RequestParam(name = "from_id", required = false) Long fromId) {
        DeferredResult<List<LongPollingUpdate>> result = new DeferredResult<>(30000L, Collections.emptyList());
        if (type != null) {
            longPollingService.registerForUpdate(result, type, fromId);
        } else {
            longPollingService.registerForUpdate(result, fromId);
        }

        return result;
    }
}
