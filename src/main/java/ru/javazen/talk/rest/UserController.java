package ru.javazen.talk.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import ru.javazen.talk.model.Group;
import ru.javazen.talk.model.Role;
import ru.javazen.talk.model.User;
import ru.javazen.talk.repository.GroupRepository;
import ru.javazen.talk.repository.RoleRepository;
import ru.javazen.talk.repository.UserRepository;
import ru.javazen.talk.rest.status.PermissionDeniedException;
import ru.javazen.talk.rest.status.ResourceNotFoundException;
import ru.javazen.talk.rest.transport.request.UserRequest;
import ru.javazen.talk.rest.transport.response.UserResponse;
import ru.javazen.talk.service.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping("/user")
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
    @Autowired
    private ConversionService conversionService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserService userService;

    @RequestMapping(path = "/{id}/get")
    public UserResponse getById(@PathVariable("id") Long id) {
        User user = userRepository.findOne(id);
        if (user == null) {
            throw new ResourceNotFoundException();
        }
        return conversionService.convert(user, UserResponse.class);
    }

    @RequestMapping(path = "/get/group{group_id}")
    public List<UserResponse> getListByGroupId(@PathVariable("group_id") Long groupId) {
        List<User> users = userRepository.findAllByGroupId(groupId);
        List<UserResponse> userResponses = new ArrayList<>();
        for (User user : users) {
            userResponses.add(conversionService.convert(user, UserResponse.class));
        }
        return userResponses;
    }

    @RequestMapping(path = "/get/all")
    public List<UserResponse> getAll() {
        Iterable<User> users = userRepository.findAll();
        List<UserResponse> userResponses = new ArrayList<>();
        for (User user : users) {
            userResponses.add(conversionService.convert(user, UserResponse.class));
        }
        return userResponses;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping("/get/role/all")
    public List<UserResponse.SimpleRole> getAllRoles() {
        Iterable<Role> roles = roleRepository.findAll();
        List<UserResponse.SimpleRole> rolesResponse = new ArrayList<>();
        for (Role role : roles) {
            UserResponse.SimpleRole simpleRole = new UserResponse.SimpleRole();
            simpleRole.setId(role.getId());
            simpleRole.setName(role.getName());
            simpleRole.setAuthority(role.getAuthority());
            rolesResponse.add(simpleRole);
        }

        return rolesResponse;
    }

    @PreAuthorize("hasRole('ADMIN')")
    @RequestMapping("/get/group/all")
    public List<UserResponse.SimpleGroup> getAllGroups() {
        Iterable<Group> groups = groupRepository.findAll();
        List<UserResponse.SimpleGroup> groupsResponse = new ArrayList<>();
        for (Group group : groups) {
            UserResponse.SimpleGroup simpleGroup = new UserResponse.SimpleGroup();
            simpleGroup.setId(group.getId());
            simpleGroup.setName(group.getName());
            groupsResponse.add(simpleGroup);
        }

        return groupsResponse;
    }

    @RequestMapping(path = "/update", method = RequestMethod.POST)
    public void updateUser(@RequestBody UserRequest userRequest) {
        LOGGER.trace("Start update user with ID = {}. Current user id = {}", userRequest.getId(), userService.getCurrentUser().getId());

        if (!userService.getCurrentUser().getId().equals(userRequest.getId())
                && !userService.hasRole("ROLE_ADMIN")) {
            LOGGER.warn("Unauthorized attempt to modify user with id = {}. Current user id = {}",
                    userRequest.getId(),
                    userService.getCurrentUser().getId());

            throw new PermissionDeniedException();
        }

        User user = userRepository.findOne(userRequest.getId());
        if (!BCrypt.checkpw(userRequest.getOldPassword(), userService.getCurrentUser().getPassword())) {
            LOGGER.debug("Failed update user data - incorrect password {} {}", userRequest.getOldPassword(), userService.getCurrentUser().getPassword());
            throw new PermissionDeniedException();
        }

        LOGGER.debug("Request user model: {}, is password changed: {}", userRequest, userRequest.getPassword() != null);

        if (userService.hasRole("ROLE_ADMIN")) {
            LOGGER.debug("Will update username, group and roles");

            user.setUsername(userRequest.getUsername());

            Group group = groupRepository.findOne(userRequest.getGroupId());
            if (group != null) {
                user.setGroup(group);
            }

            Iterable<Role> roleIterable = roleRepository.findAll(userRequest.getRoleIds());
            Set<Role> roles = StreamSupport.stream(roleIterable.spliterator(), false)
                    .collect(Collectors.toSet());
            user.setRoles(roles);
        }
        if (userRequest.getPassword() != null && !userRequest.getPassword().isEmpty()) {
            LOGGER.debug("Change password");
            user.setPassword(passwordEncoder.encode(userRequest.getPassword()));
        }
        user.setFullName(userRequest.getFullName());
        user.setDescription(userRequest.getDescription());

        userRepository.save(user);
    }

    @RequestMapping(path = "/password/check", method = RequestMethod.POST)
    public ResponseEntity<Boolean> checkPassword(@RequestBody String password) {
        boolean result = BCrypt.checkpw(password, userService.getCurrentUser().getPassword());

        return ResponseEntity.ok(result);
    }

    @RequestMapping(path = "/username/free", method = RequestMethod.GET)
    public ResponseEntity<Boolean> hasUser(@RequestParam("username") String username) {
        boolean result = userRepository.findByUsernameIgnoreCase(username) == null;

        return ResponseEntity.ok(result);
    }

}
