package ru.javazen.talk.rest.transport.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

/**
 * Created by Andrew on 15.04.2017.
 */
public class ChatUpdateResponse {

    private Long id;

    private String message;

    @JsonProperty("sent_when")
    private Date sentWhen;

    @JsonProperty("user_id")
    private Long userId;

    @JsonProperty("chat_id")
    private Long chatId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getSentWhen() {
        return sentWhen;
    }

    public void setSentWhen(Date sentWhen) {
        this.sentWhen = sentWhen;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getChatId() {
        return chatId;
    }

    public void setChatId(Long chatId) {
        this.chatId = chatId;
    }
}