package ru.javazen.talk.rest.transport.response;

/**
 * Created by Andrew on 01.03.2017.
 */
public class TypeOfRelationBetweenKeywordsResponse {

    private String name;
    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
