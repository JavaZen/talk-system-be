package ru.javazen.talk.rest.transport.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Set;

public class ResponseResponse {

    private Long id;

    @JsonProperty("knowledge_base_id")
    private Long knowledgeBaseId;

    @JsonProperty("is_root")
    private Boolean isRoot;

    private String text;

    private ResponseTypeResponse type;

    @JsonProperty("context_behavior")
    private ContextBehaviorResponse contextBehaviorResponse;

    private Set<Flag> flags;

    public static class Flag {
        private String name;
        private String value;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getKnowledgeBaseId() {
        return knowledgeBaseId;
    }

    public void setKnowledgeBaseId(Long knowledgeBaseId) {
        this.knowledgeBaseId = knowledgeBaseId;
    }

    public Boolean getRoot() {
        return isRoot;
    }

    public void setRoot(Boolean root) {
        isRoot = root;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ResponseTypeResponse getType() {
        return type;
    }

    public void setType(ResponseTypeResponse type) {
        this.type = type;
    }

    public ContextBehaviorResponse getContextBehaviorResponse() {
        return contextBehaviorResponse;
    }

    public void setContextBehaviorResponse(ContextBehaviorResponse contextBehaviorResponse) {
        this.contextBehaviorResponse = contextBehaviorResponse;
    }

    public Set<Flag> getFlags() {
        return flags;
    }

    public void setFlags(Set<Flag> flags) {
        this.flags = flags;
    }
}
