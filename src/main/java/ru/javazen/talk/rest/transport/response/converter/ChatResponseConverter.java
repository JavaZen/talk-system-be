package ru.javazen.talk.rest.transport.response.converter;

import org.springframework.core.convert.converter.Converter;
import ru.javazen.talk.model.Chat;
import ru.javazen.talk.model.ChatParticipant;
import ru.javazen.talk.rest.transport.response.ChatResponse;

import java.util.ArrayList;

/**
 * Created by Andrew on 04.06.2017.
 */
public class ChatResponseConverter implements Converter<Chat, ChatResponse> {

    @Override
    public ChatResponse convert(Chat chat) {
        ChatResponse response = new ChatResponse();
        response.setId(chat.getId());
        response.setTitle(chat.getTitle());
        response.setParticipants(new ArrayList<>());
        for (ChatParticipant participant : chat.getChatParticipants()) {
            ChatResponse.SimpleParticipant simpleParticipant = new ChatResponse.SimpleParticipant();
            simpleParticipant.setId(participant.getId());
            simpleParticipant.setUserId(participant.getUser().getId());
            simpleParticipant.setUsername(participant.getUser().getUsername());
            simpleParticipant.setChatRole(participant.getParticipantRole().name());
            response.getParticipants().add(simpleParticipant);
        }

        return response;
    }
}
