package ru.javazen.talk.rest.transport.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ChatUpdateRequest {

    @JsonProperty("chat_id")
    private Long chatId;

    private String message;

    @JsonProperty("random_id")
    private Long randomId;

    public Long getChatId() {
        return chatId;
    }

    public void setChatId(Long chatId) {
        this.chatId = chatId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Long getRandomId() {
        return randomId;
    }

    public void setRandomId(Long randomId) {
        this.randomId = randomId;
    }
}
