package ru.javazen.talk.rest.transport.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;

public class ChatRequest {

    private String title;

    @JsonProperty("is_group")
    private Boolean isGroup;

    private List<Long> members;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getGroup() {
        return isGroup;
    }

    public void setGroup(Boolean group) {
        isGroup = group;
    }

    public List<Long> getMembers() {
        return members;
    }

    public void setMembers(List<Long> members) {
        this.members = members;
    }

}
