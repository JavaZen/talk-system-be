package ru.javazen.talk.rest.transport.response.converter;

import org.springframework.core.convert.converter.Converter;
import ru.javazen.talk.model.Group;
import ru.javazen.talk.rest.transport.response.GroupResponse;

public class GroupResponseConverter implements Converter<Group, GroupResponse> {

    @Override
    public GroupResponse convert(Group group) {
        GroupResponse response = new GroupResponse();
        response.setId(group.getId());
        response.setName(group.getName());
        response.setDescription(group.getDescription());

        return response;
    }
}
