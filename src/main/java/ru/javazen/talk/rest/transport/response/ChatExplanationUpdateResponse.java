package ru.javazen.talk.rest.transport.response;

import ru.javazen.talk.explanation.data.Base;

/**
 * Created by Andrew on 25.05.2017.
 */
public class ChatExplanationUpdateResponse {

    private Base base;
    private Long chatId;

    public Base getBase() {
        return base;
    }

    public void setBase(Base base) {
        this.base = base;
    }

    public Long getChatId() {
        return chatId;
    }

    public void setChatId(Long chatId) {
        this.chatId = chatId;
    }
}
