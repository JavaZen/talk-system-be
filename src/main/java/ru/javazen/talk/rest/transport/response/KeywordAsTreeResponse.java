package ru.javazen.talk.rest.transport.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Set;

public class KeywordAsTreeResponse {

    private Long id;

    private String text;

    private Set<Flag> flags;

    public static class  Flag {
        private String name;
        private String value;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    @JsonProperty("knowledge_base_id")
    private Long knowledgeBaseId;

    @JsonProperty("related_keywords")
    private List<KeywordRelation> keywordRelations;

    public static class KeywordRelation {

        @JsonProperty("relation_id")
        private Long relationId;

        @JsonProperty("type_of_relation")
        private TypeOfRelationBetweenKeywordsResponse typeOfRelation;

        @JsonProperty("keyword")
        private KeywordAsTreeResponse keywordAsTreeResponse;

        public Long getRelationId() {
            return relationId;
        }

        public void setRelationId(Long relationId) {
            this.relationId = relationId;
        }

        public TypeOfRelationBetweenKeywordsResponse getTypeOfRelation() {
            return typeOfRelation;
        }

        public void setTypeOfRelation(TypeOfRelationBetweenKeywordsResponse typeOfRelation) {
            this.typeOfRelation = typeOfRelation;
        }

        public KeywordAsTreeResponse getKeywordAsTreeResponse() {
            return keywordAsTreeResponse;
        }

        public void setKeywordAsTreeResponse(KeywordAsTreeResponse keywordAsTreeResponse) {
            this.keywordAsTreeResponse = keywordAsTreeResponse;
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Set<Flag> getFlags() {
        return flags;
    }

    public void setFlags(Set<Flag> flags) {
        this.flags = flags;
    }

    public Long getKnowledgeBaseId() {
        return knowledgeBaseId;
    }

    public void setKnowledgeBaseId(Long knowledgeBaseId) {
        this.knowledgeBaseId = knowledgeBaseId;
    }

    public List<KeywordRelation> getKeywordRelations() {
        return keywordRelations;
    }

    public void setKeywordRelations(List<KeywordRelation> keywordRelations) {
        this.keywordRelations = keywordRelations;
    }
}
