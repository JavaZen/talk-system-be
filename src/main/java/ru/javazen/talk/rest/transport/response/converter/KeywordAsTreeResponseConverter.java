package ru.javazen.talk.rest.transport.response.converter;

import org.springframework.core.convert.converter.Converter;
import ru.javazen.talk.model.Keyword;
import ru.javazen.talk.rest.transport.response.KeywordAsTreeResponse;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Andrew on 22.05.2017.
 */
public class KeywordAsTreeResponseConverter implements Converter<Keyword, KeywordAsTreeResponse> {

    @Override
    public KeywordAsTreeResponse convert(Keyword keyword) {
        KeywordAsTreeResponse keywordAsTreeResponse = new KeywordAsTreeResponse();
        keywordAsTreeResponse.setId(keyword.getId());
        keywordAsTreeResponse.setText(keyword.getText());
        if (keyword.getKnowledgeBase() != null) {
            keywordAsTreeResponse.setKnowledgeBaseId(keyword.getKnowledgeBase().getId());
        }
        if (keyword.getFlags() != null) {
            Set<KeywordAsTreeResponse.Flag> flags = new HashSet<>();
            for (Keyword.Flag flag : keyword.getFlags()) {
                KeywordAsTreeResponse.Flag flagResp = new KeywordAsTreeResponse.Flag();
                flagResp.setName(flag.getName());
                flagResp.setValue(flag.getValue());
                flags.add(flagResp);
            }
            keywordAsTreeResponse.setFlags(flags);
        }

        return keywordAsTreeResponse;
    }
}
