package ru.javazen.talk.rest.transport.request;

/**
 * Created by Andrew on 07.05.2017.
 */
public class SingleValue {

    private String value;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
