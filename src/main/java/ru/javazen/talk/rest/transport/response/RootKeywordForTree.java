package ru.javazen.talk.rest.transport.response;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Andrew on 11.03.2017.
 */
public class RootKeywordForTree {

    private Boolean root = true;

    @JsonProperty("keyword")
    private KeywordAsTreeResponse keywordAsTreeResponse;

    public Boolean getRoot() {
        return root;
    }

    public void setRoot(Boolean root) {
        this.root = root;
    }

    public KeywordAsTreeResponse getKeywordAsTreeResponse() {
        return keywordAsTreeResponse;
    }

    public void setKeywordAsTreeResponse(KeywordAsTreeResponse keywordAsTreeResponse) {
        this.keywordAsTreeResponse = keywordAsTreeResponse;
    }
}
