package ru.javazen.talk.rest.transport.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by Andrew on 22.04.2017.
 */
public class ChatResponse {

    private Long id;
    private String title;
    private List<SimpleParticipant> participants;

    public static class SimpleParticipant {

        private Long id;

        @JsonProperty("user_id")
        private Long userId;

        private String username;

        @JsonProperty("chat_role")
        private String chatRole;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public Long getUserId() {
            return userId;
        }

        public void setUserId(Long userId) {
            this.userId = userId;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getChatRole() {
            return chatRole;
        }

        public void setChatRole(String chatRole) {
            this.chatRole = chatRole;
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<SimpleParticipant> getParticipants() {
        return participants;
    }

    public void setParticipants(List<SimpleParticipant> participants) {
        this.participants = participants;
    }
}
