package ru.javazen.talk.rest.transport.request;

import com.fasterxml.jackson.annotation.JsonProperty;

public class KeywordToResponseRequest {

    @JsonProperty("relation_id")
    private Long relationId;

    @JsonProperty("keyword_id")
    private Long keywordId;

    @JsonProperty("response_id")
    private Long responseId;

    public Long getRelationId() {
        return relationId;
    }

    public void setRelationId(Long relationId) {
        this.relationId = relationId;
    }

    public Long getKeywordId() {
        return keywordId;
    }

    public void setKeywordId(Long keywordId) {
        this.keywordId = keywordId;
    }

    public Long getResponseId() {
        return responseId;
    }

    public void setResponseId(Long responseId) {
        this.responseId = responseId;
    }
}