package ru.javazen.talk.rest.transport.request;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by Andrew on 06.01.2017.
 */
public class KeywordToKeywordRequest {

    @JsonProperty("relation_id")
    private Long relationId;

    @JsonProperty("keyword_from_id")
    private Long keywordFromId;

    @JsonProperty("keyword_to_id")
    private Long keywordToId;

    @JsonProperty("type_of_relation")
    private TypeOfRelation typeOfRelation;

    public static class TypeOfRelation {
        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public Long getRelationId() {
        return relationId;
    }

    public void setRelationId(Long relationId) {
        this.relationId = relationId;
    }

    public Long getKeywordFromId() {
        return keywordFromId;
    }

    public void setKeywordFromId(Long keywordFromId) {
        this.keywordFromId = keywordFromId;
    }

    public Long getKeywordToId() {
        return keywordToId;
    }

    public void setKeywordToId(Long keywordToId) {
        this.keywordToId = keywordToId;
    }

    public TypeOfRelation getTypeOfRelation() {
        return typeOfRelation;
    }

    public void setTypeOfRelation(TypeOfRelation typeOfRelation) {
        this.typeOfRelation = typeOfRelation;
    }
}
