package ru.javazen.talk.rest.transport.response.converter;

import org.springframework.core.convert.converter.Converter;
import ru.javazen.talk.model.Role;
import ru.javazen.talk.model.User;
import ru.javazen.talk.rest.transport.response.UserResponse;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Andrew on 04.01.2017.
 */
public class UserResponseConverter implements Converter<User, UserResponse> {

    @Override
    public UserResponse convert(User user) {
        UserResponse response = new UserResponse();
        response.setId(user.getId());
        response.setUsername(user.getUsername());
        response.setFullName(user.getFullName());
        response.setDescription(user.getDescription());

        UserResponse.SimpleGroup group = new UserResponse.SimpleGroup();
        group.setId(user.getGroup().getId());
        group.setName(user.getGroup().getName());
        response.setGroup(group);

        List<UserResponse.SimpleRole> roles = new ArrayList<>();
        for (Role role : user.getRoles()) {
            UserResponse.SimpleRole simpleRole = new UserResponse.SimpleRole();
            simpleRole.setId(role.getId());
            simpleRole.setName(role.getName());
            simpleRole.setAuthority(role.getAuthority());
            roles.add(simpleRole);
        }
        response.setRoles(roles);

        return response;
    }
}
