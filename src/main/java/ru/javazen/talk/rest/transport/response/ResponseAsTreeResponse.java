package ru.javazen.talk.rest.transport.response;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Set;

/**
 * Created by Andrew on 08.05.2017.
 */
public class ResponseAsTreeResponse {
    private Long id;

    private String text;

    private ResponseTypeResponse type;

    @JsonProperty("knowledge_base_id")
    private Long knowledgeBaseId;

    @JsonProperty("is_root")
    private Boolean isRoot;

    @JsonProperty("related_responses")
    List<ResponseRelation> responseRelations;

    private Set<Flag> flags;

    @JsonProperty("context_behavior")
    private ContextBehaviorResponse contextBehaviorResponse;

    public static class Flag {
        private String name;

        private String value;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    public static class ResponseRelation {

        @JsonProperty("relation_id")
        private Long relationId;

        private ResponseAsTreeResponse response;

        public Long getRelationId() {
            return relationId;
        }

        public void setRelationId(Long relationId) {
            this.relationId = relationId;
        }

        public ResponseAsTreeResponse getResponse() {
            return response;
        }

        public void setResponse(ResponseAsTreeResponse response) {
            this.response = response;
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<ResponseRelation> getResponseRelations() {
        return responseRelations;
    }

    public void setResponseRelations(List<ResponseRelation> responseRelations) {
        this.responseRelations = responseRelations;
    }

    public ResponseTypeResponse getType() {
        return type;
    }

    public void setType(ResponseTypeResponse type) {
        this.type = type;
    }

    public Long getKnowledgeBaseId() {
        return knowledgeBaseId;
    }

    public void setKnowledgeBaseId(Long knowledgeBaseId) {
        this.knowledgeBaseId = knowledgeBaseId;
    }

    public Boolean getRoot() {
        return isRoot;
    }

    public void setRoot(Boolean root) {
        isRoot = root;
    }

    public Set<Flag> getFlags() {
        return flags;
    }

    public void setFlags(Set<Flag> flags) {
        this.flags = flags;
    }

    public ContextBehaviorResponse getContextBehaviorResponse() {
        return contextBehaviorResponse;
    }

    public void setContextBehaviorResponse(ContextBehaviorResponse contextBehaviorResponse) {
        this.contextBehaviorResponse = contextBehaviorResponse;
    }
}
