package ru.javazen.talk.rest.transport.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import ru.javazen.talk.rest.transport.response.ContextBehaviorResponse;

import java.util.List;
import java.util.Set;

/**
 * Created by Andrew on 25.03.2017.
 */
public class ResponseRequest {

    private Long id;

    private String text;

    private Type type;

    private List<KeywordRequest> keywords;

    @JsonProperty("parent_id")
    private Long parentId;

    @JsonProperty("context_behavior")
    private ContextBehaviorResponse contextBehaviorResponse;

    @JsonProperty("is_root")
    private Boolean isRoot;

    private Set<Flag> flags;

    @JsonProperty("knowledge_base_id")
    private Long knowledgeBaseId;

    public static class Flag {
        private String name;

        private String value;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    public static class Type {
        private String name;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Long getKnowledgeBaseId() {
        return knowledgeBaseId;
    }

    public void setKnowledgeBaseId(Long knowledgeBaseId) {
        this.knowledgeBaseId = knowledgeBaseId;
    }

    public List<KeywordRequest> getKeywords() {
        return keywords;
    }

    public void setKeywords(List<KeywordRequest> keywords) {
        this.keywords = keywords;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public ContextBehaviorResponse getContextBehaviorResponse() {
        return contextBehaviorResponse;
    }

    public void setContextBehaviorResponse(ContextBehaviorResponse contextBehaviorResponse) {
        this.contextBehaviorResponse = contextBehaviorResponse;
    }

    public Boolean getRoot() {
        return isRoot;
    }

    public void setRoot(Boolean root) {
        isRoot = root;
    }

    public Set<Flag> getFlags() {
        return flags;
    }

    public void setFlags(Set<Flag> flags) {
        this.flags = flags;
    }
}
