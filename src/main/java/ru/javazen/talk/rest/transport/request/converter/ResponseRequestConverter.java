package ru.javazen.talk.rest.transport.request.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import ru.javazen.talk.model.KnowledgeBase;
import ru.javazen.talk.model.Response;
import ru.javazen.talk.repository.KnowledgeBaseRepository;
import ru.javazen.talk.rest.transport.request.ResponseRequest;

import java.util.HashSet;
import java.util.Set;

public class ResponseRequestConverter implements Converter<ResponseRequest, Response> {

    @Autowired
    private KnowledgeBaseRepository knowledgeBaseRepository;

    @Override
    public Response convert(ResponseRequest responseRequest) {
        Response response = new Response();
        response.setText(responseRequest.getText());
        response.setType(Response.Type.valueOf(responseRequest.getType().getName()));

        Set<Response.Flag> flags = new HashSet<>();
        responseRequest.getFlags().forEach(flag -> {
            Response.Flag flagData = new Response.Flag();
            flagData.setValue(flag.getValue());
            flagData.setName(flag.getName());

            flags.add(flagData);
        });

        response.setFlags(flags);
        KnowledgeBase knowledgeBase = knowledgeBaseRepository.findOne(responseRequest.getKnowledgeBaseId());
        response.setKnowledgeBase(knowledgeBase);

        response.setRoot(responseRequest.getRoot());

        return response;
    }
}
