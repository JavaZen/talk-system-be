package ru.javazen.talk.rest.transport.response;

/**
 * Created by Andrew on 09.05.2017.
 */
public class RootResponseForTree {

    ResponseAsTreeResponse response;

    public ResponseAsTreeResponse getResponse() {
        return response;
    }

    public void setResponse(ResponseAsTreeResponse response) {
        this.response = response;
    }
}
