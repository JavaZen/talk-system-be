package ru.javazen.talk.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.javazen.talk.model.User;
import ru.javazen.talk.rest.status.NotAuthorizedException;
import ru.javazen.talk.rest.transport.request.UserRequest;
import ru.javazen.talk.rest.transport.response.UserResponse;
import ru.javazen.talk.service.UserService;

import java.security.Principal;

@RestController
@RequestMapping(path = "/auth")
public class AuthController {

    @Autowired
    private UserService userService;

    @Autowired
    private ConversionService conversionService;

    @RequestMapping("/user")
    public ResponseEntity<UserResponse> user(Principal principal) {
        UserResponse response;

        if (principal == null) {
            throw new NotAuthorizedException();
        }
        try {
            User user = userService.loadUserByUsername(principal.getName());
             response = conversionService.convert(user, UserResponse.class);
        } catch (UsernameNotFoundException e) {
            throw new NotAuthorizedException();
        }

        return ResponseEntity.ok(response);
    }

    @RequestMapping(path = "/signup", method = RequestMethod.POST)
    public void signup(@RequestBody UserRequest userRequest) {

        User user = new User();
        user.setUsername(userRequest.getUsername());
        user.setPassword(userRequest.getPassword());
        user.setFullName(userRequest.getFullName());
        user.setDescription(userRequest.getDescription());

        userService.signupUser(user);
    }

}
