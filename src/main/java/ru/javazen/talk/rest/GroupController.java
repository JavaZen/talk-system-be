package ru.javazen.talk.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.javazen.talk.model.Group;
import ru.javazen.talk.repository.GroupRepository;
import ru.javazen.talk.rest.status.ResourceNotFoundException;
import ru.javazen.talk.rest.transport.request.GroupRequest;
import ru.javazen.talk.rest.transport.response.GroupResponse;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/group")
public class GroupController {

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    ConversionService conversionService;

    @RequestMapping(path = "/{id}/get")
    public GroupResponse getById(@PathVariable("id") Long id) {
        Group group = groupRepository.findOne(id);

        return conversionService.convert(group, GroupResponse.class);
    }

    @RequestMapping(path = "/get/all")
    public List<GroupResponse> getAll() {
        Iterable<Group> groups = groupRepository.findAll();

        List<GroupResponse> responses = new ArrayList<>();
        for (Group group : groups) {
            responses.add(conversionService.convert(group, GroupResponse.class));
        }

        return responses;
    }

    @RequestMapping(path = "/add")
    public void add(@RequestBody GroupRequest groupRequest) {
        if (groupRequest.getId() != null) {
            return;
        }

        Group group = new Group();

        group.setName(groupRequest.getName());
        group.setDescription(groupRequest.getDescription());

        groupRepository.save(group);
    }

    @RequestMapping(path = "/update")
    public void update(@RequestBody GroupRequest groupRequest) {
        Group group = groupRepository.findOne(groupRequest.getId());
        if (group == null) {
            throw new ResourceNotFoundException();
        }

        group.setName(groupRequest.getName());
        group.setDescription(groupRequest.getDescription());

        groupRepository.save(group);
    }

    @RequestMapping(path = "/{id}/delete")
    public void delete(@PathVariable("id") Long id) {
        groupRepository.delete(id);
    }
}
