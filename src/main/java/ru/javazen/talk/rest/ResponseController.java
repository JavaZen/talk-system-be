package ru.javazen.talk.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import ru.javazen.talk.model.*;
import ru.javazen.talk.repository.*;
import ru.javazen.talk.rest.transport.request.KeywordRequest;
import ru.javazen.talk.rest.transport.request.ResponseRequest;
import ru.javazen.talk.rest.transport.response.*;

import java.util.*;
import java.util.stream.Collectors;

@RestController
@RequestMapping("response")
public class ResponseController {

    @Autowired
    private ResponseRepository responseRepository;

    @Autowired
    private KeywordToResponseRepository keywordToResponseRepository;

    @Autowired
    private KnowledgeBaseRepository knowledgeBaseRepository;

    @Autowired
    private ResponseToResponseRepository responseToResponseRepository;

    @Autowired
    private KeywordRepository keywordRepository;

    @RequestMapping(path = "/get/all/kb{knowledgeBaseId}")
    public List<ResponseResponse> getResponsesByKbId(@PathVariable Long knowledgeBaseId) {
        List<Response> responses = responseRepository.getResponsesByKnowledgeBaseId(knowledgeBaseId);
        List<ResponseResponse> responseResponses = new ArrayList<>();

        for (Response response : responses) {
            ResponseResponse responseResponse = new ResponseResponse();
            responseResponse.setId(response.getId());
            responseResponse.setText(response.getText());
            responseResponse.setRoot(response.getRoot());

            ResponseTypeResponse responseTypeResponse = new ResponseTypeResponse();
            responseTypeResponse.setName(response.getType().name());
            responseTypeResponse.setValue(response.getType().getValue());
            responseResponse.setType(responseTypeResponse);
            responseResponse.setKnowledgeBaseId(knowledgeBaseId);

            responseResponses.add(responseResponse);
        }

        return responseResponses;
    }

    @RequestMapping(path = "/add", method = RequestMethod.POST)
    public ResponseEntity<ResponseAsTreeResponse> addResponse(@RequestBody ResponseRequest responseRequest) {

        Response response = new Response();
        response.setText(responseRequest.getText());
        response.setType(Response.Type.valueOf(responseRequest.getType().getName()));

        Set<Response.Flag> flags = new HashSet<>();
        responseRequest.getFlags().forEach(flag -> {
            Response.Flag flagData = new Response.Flag();
            flagData.setValue(flag.getValue());
            flagData.setName(flag.getName());

            flags.add(flagData);
        });

        response.setFlags(flags);
        KnowledgeBase knowledgeBase = knowledgeBaseRepository.findOne(responseRequest.getKnowledgeBaseId());
        response.setKnowledgeBase(knowledgeBase);

        response.setRoot(responseRequest.getRoot());
        response.setContextBehavior(Response.ContextBehavior.valueOf(responseRequest.getContextBehaviorResponse().getName()));

        responseRepository.save(response);


        List<Long> keywordIds = responseRequest.getKeywords().stream().map(KeywordRequest::getId).collect(Collectors.toList());
        Iterable<Keyword> keywords = keywordRepository.findAll(keywordIds);

        //keywordToResponseRepository.removeKeywordRelationsByResponseId(response.getId());
        for (Keyword keyword : keywords) {
            KeywordToResponse keywordToResponse = new KeywordToResponse();
            keywordToResponse.setKeyword(keyword);
            keywordToResponse.setResponse(response);
            keywordToResponseRepository.save(keywordToResponse);
        }

        if (responseRequest.getParentId() != null) {
            Response parent = responseRepository.findOne(responseRequest.getParentId());
            ResponseToResponse r2r = new ResponseToResponse();
            r2r.setResponseFrom(parent);
            r2r.setResponseTo(response);

            responseToResponseRepository.save(r2r);
        }

        return ResponseEntity.ok(buildTree(response));
    }

    @Transactional
    @RequestMapping(path = "/update", method = RequestMethod.POST)
    public ResponseEntity<Long> updateResponse(@RequestBody ResponseRequest responseRequest) {

        Response response = responseRepository.findOne(responseRequest.getId());
        response.setText(responseRequest.getText());
        response.setType(Response.Type.valueOf(responseRequest.getType().getName()));

        Set<Response.Flag> flags = new HashSet<>();
        responseRequest.getFlags().forEach(flag -> {
            Response.Flag flagData = new Response.Flag();
            flagData.setValue(flag.getValue());
            flagData.setName(flag.getName());

            flags.add(flagData);
        });

        response.setFlags(flags);

        KnowledgeBase knowledgeBase = knowledgeBaseRepository.findOne(responseRequest.getKnowledgeBaseId());
        response.setKnowledgeBase(knowledgeBase);
        response.setContextBehavior(Response.ContextBehavior.valueOf(responseRequest.getContextBehaviorResponse().getName()));

        responseRepository.save(response);

        List<Long> keywordIds = responseRequest.getKeywords().stream().map(KeywordRequest::getId).collect(Collectors.toList());
        Iterable<Keyword> keywords = keywordRepository.findAll(keywordIds);

        keywordToResponseRepository.removeKeywordRelationsByResponseId(response.getId());
        //Response response = responseRepository.findOne(responseId);

        for (Keyword keyword : keywords) {
            KeywordToResponse keywordToResponse = new KeywordToResponse();
            keywordToResponse.setKeyword(keyword);
            keywordToResponse.setResponse(response);
            keywordToResponseRepository.save(keywordToResponse);
        }

        return ResponseEntity.ok(response.getId());
    }

    @Transactional
    @RequestMapping(path = "{id}/delete", method = RequestMethod.POST)
    public void deleteResponse(@PathVariable("id") Long id) {
        Response response = responseRepository.findOne(id);
        deleteTree(response);
    }

    private void deleteTree(Response response) {
        List<ResponseToResponse> r2rs = responseToResponseRepository.getRelationsByParentId(response.getId());

        for (ResponseToResponse r2r : r2rs) {
            deleteTree(r2r.getResponseTo());
            responseToResponseRepository.delete(r2r);
        }
        keywordToResponseRepository.removeKeywordRelationsByResponseId(response.getId());

        responseRepository.delete(response);
    }

    @RequestMapping("/get/tree/kb{knowledgeBaseId}")
    public List<RootResponseForTree> getTreeByKnowledgeBaseId(@PathVariable Long knowledgeBaseId) {
        List<Response> responses = responseRepository.getResponsesByKnowledgeBaseIdAndIsRootTrue(knowledgeBaseId);
        List<RootResponseForTree> responseResponses = new ArrayList<>();
        for(Response response : responses) {
            responseResponses.add(new RootResponseForTree() {{ setResponse(buildTree(response)); }});
        }

        return responseResponses;
    }

    @RequestMapping(path = "/types/get/all", method = RequestMethod.GET)
    public List<ResponseTypeResponse> getPathsOfSpeech() {

        List<ResponseTypeResponse> responses = new ArrayList<>();
        for (Response.Type type : Response.Type.values()) {
            ResponseTypeResponse response = new ResponseTypeResponse();
            response.setName(type.name());
            response.setValue(type.getValue());

            responses.add(response);
        }

        return responses;
    }

    @RequestMapping(path = "/typesOfRelations/get/all", method = RequestMethod.GET)
    public List<ContextBehaviorResponse> getTypesOfRelation() {

        List<ContextBehaviorResponse> responses = new ArrayList<>();
        for (Response.ContextBehavior contextBehavior : Response.ContextBehavior.values()) {
            ContextBehaviorResponse response = new ContextBehaviorResponse();
            response.setName(contextBehavior.name());
            response.setValue(contextBehavior.getValue());

            responses.add(response);
        }

        return responses;
    }


    @RequestMapping(path = "/flags/get/all", method = RequestMethod.GET)
    public Set<ResponseResponse.Flag> getAllFlags() {
        Set<ResponseResponse.Flag> flags = new HashSet<>();

        ResponseResponse.Flag answer = new ResponseResponse.Flag();
        answer.setValue("Ответ");
        answer.setName("ANSWER");

        ResponseResponse.Flag question = new ResponseResponse.Flag();
        question.setValue("Вопрос");
        question.setName("QUESTION");

        ResponseResponse.Flag impasse = new ResponseResponse.Flag();
        impasse.setValue("Обработчик тупика");
        impasse.setName("IMPASSE_HANDLER");

        ResponseResponse.Flag terminal = new ResponseResponse.Flag();
        terminal.setValue("Терминальный");
        terminal.setName("TERMINAL");

        Collections.addAll(flags, answer, question, impasse, terminal);
        return flags;
    }

    private ResponseAsTreeResponse buildTree(Response response) {
        ResponseAsTreeResponse responseAsTreeResponse = new ResponseAsTreeResponse();
        responseAsTreeResponse.setId(response.getId());
        responseAsTreeResponse.setText(response.getText());
        responseAsTreeResponse.setRoot(response.getRoot());
        responseAsTreeResponse.setContextBehaviorResponse(new ContextBehaviorResponse() {{
            setName(response.getContextBehavior().name());
            setValue(response.getContextBehavior().getValue());
        }});

        ResponseTypeResponse responseTypeResponse = new ResponseTypeResponse();
        responseTypeResponse.setName(response.getType().name());
        responseTypeResponse.setValue(response.getType().getValue());

        responseAsTreeResponse.setType(responseTypeResponse);

        Set<ResponseAsTreeResponse.Flag> flags = new HashSet<>();
        response.getFlags().forEach(flag -> {
            ResponseAsTreeResponse.Flag responseFlag = new ResponseAsTreeResponse.Flag();
            responseFlag.setName(flag.getName());
            responseFlag.setValue(flag.getValue());

            flags.add(responseFlag);
        });
        responseAsTreeResponse.setFlags(flags);

        if (response.getKnowledgeBase() != null) {
            responseAsTreeResponse.setKnowledgeBaseId(response.getKnowledgeBase().getId());
        }

        List<ResponseToResponse> responseToResponses
                = responseToResponseRepository.getRelationsByParentId(response.getId());

        List<ResponseAsTreeResponse.ResponseRelation> responseRelations = new ArrayList<>();
        for (ResponseToResponse responseToResponse : responseToResponses) {
            ResponseAsTreeResponse.ResponseRelation responseRelation = new ResponseAsTreeResponse.ResponseRelation();
            responseRelation.setRelationId(responseToResponse.getId());

            ResponseAsTreeResponse ra3r = buildTree(responseToResponse.getResponseTo());

            responseRelation.setResponse(ra3r);

            responseRelations.add(responseRelation);
        }
        responseAsTreeResponse.setResponseRelations(responseRelations);

        return responseAsTreeResponse;
    }

}