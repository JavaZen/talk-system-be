package ru.javazen.talk.security.evaluator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.javazen.talk.model.KnowledgeBase;
import ru.javazen.talk.model.KnowledgeBaseGrant;
import ru.javazen.talk.model.User;
import ru.javazen.talk.repository.KnowledgeBaseGrantRepository;

import java.io.Serializable;

public class ZenPermissionEvaluator implements PermissionEvaluator {

    @Autowired
    private KnowledgeBaseGrantRepository knowledgeBaseGrantRepository;

    //@PostAuthorize("hasPermission(returnObject, 'read')")
    @Override
    public boolean hasPermission(
            Authentication auth, Object targetDomainObject, Object permission) {
        if ((auth == null) || (targetDomainObject == null) || !(permission instanceof String)){
            return false;
        }

        if (targetDomainObject instanceof KnowledgeBase) {
            KnowledgeBase base = (KnowledgeBase) targetDomainObject;
            Long kbId = base.getId();

            UserDetails userDetails = (UserDetails) auth.getPrincipal();

            if (userDetails instanceof User) {
                User user = (User) userDetails;
                return checkAccessOnKb(kbId, user.getId(), (String)permission);
            }        }

        return false;
    }

    //@PreAuthorize("hasPermission(#id, 'Foo', 'read')")
    @Override
    public boolean hasPermission(
            Authentication auth, Serializable targetId, String targetType, Object permission) {
        if ((auth == null) || (targetType == null) || !(permission instanceof String)) {
            return false;
        }

        if (targetType.equalsIgnoreCase(KnowledgeBase.class.getName())) {
            Long kbId = (Long) targetId;

            UserDetails userDetails = (UserDetails) auth.getPrincipal();

            if (userDetails instanceof User) {
                User user = (User) userDetails;
                return checkAccessOnKb(kbId, user.getId(), (String)permission);
            }
        }

        return false;
    }


    private boolean checkAccessOnKb(Long kbId, Long userId, String permission) {
        KnowledgeBaseGrant grant = knowledgeBaseGrantRepository.findByKnowledgeBaseIdAndUserId(kbId, userId);

        switch ((String)permission) {
            case "read":
                return grant.getGrant() == KnowledgeBaseGrant.Grant.READ || grant.getGrant() == KnowledgeBaseGrant.Grant.WRITE;
            case "write":
                return grant.getGrant() == KnowledgeBaseGrant.Grant.WRITE;
        }
        return false;
    }
}
