package ru.javazen.talk.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "RESPONSES")
public class Response {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne
    @JoinColumn(name = "KNOWLEDGE_BASE_ID")
    private KnowledgeBase knowledgeBase;

    @Column(name = "TEXT")
    private String text;

    @Column(name = "TYPE")
    @Enumerated(EnumType.STRING)
    private Type type;

    @Column(name = "CONTEXT_BEHAVIOR")
    @Enumerated(EnumType.STRING)
    private ContextBehavior contextBehavior;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(
            name="RESPONSE_FLAGS",
            joinColumns=@JoinColumn(name="RESPONSE_ID")
    )
    @Column(name="FLAGS")
    private Set<Flag> flags;

    @Column(name = "IS_ROOT")
    private Boolean isRoot;

    public enum Type {
        REPEATABLE("Повторяемый"),
        NOT_REPEATABLE("Не повторяемый"),
        NOT_REPEATABLE_IN_CONTEXT("Не повторяемый в контексте");

        private String value;

        Type(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public enum ContextBehavior {
        KEEP_CONTEXT("Сохранять текущий контекст"),
        USE_PARENT_CONTEXT("Использовать родительский контекст"),
        USE_ROOT_CONTEXT("Использовать корневой контекст");

        private String value;

        ContextBehavior(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    @Embeddable
    public static class Flag {
        @Column(name = "NAME")
        String name;

        @Column(name = "VALUE")
        String value;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public KnowledgeBase getKnowledgeBase() {
        return knowledgeBase;
    }

    public void setKnowledgeBase(KnowledgeBase knowledgeBase) {
        this.knowledgeBase = knowledgeBase;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public ContextBehavior getContextBehavior() {
        return contextBehavior;
    }

    public void setContextBehavior(ContextBehavior contextBehavior) {
        this.contextBehavior = contextBehavior;
    }

    public Set<Flag> getFlags() {
        return flags;
    }

    public void setFlags(Set<Flag> flags) {
        this.flags = flags;
    }

    public Boolean getRoot() {
        return isRoot;
    }

    public void setRoot(Boolean root) {
        isRoot = root;
    }
}
