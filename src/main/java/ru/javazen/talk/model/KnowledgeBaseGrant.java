package ru.javazen.talk.model;

import ru.javazen.talk.model.pk.KnowledgeBaseGrantPk;

import javax.persistence.*;

@Entity
@Table(name = "KNOWLEDGE_BASE_GRANT")
@IdClass(KnowledgeBaseGrantPk.class)
public class KnowledgeBaseGrant {

    @Id
    @Column(name = "KNOWLEDGE_BASE_ID")
    private Long knowledgeBaseId;

    @Id
    @Column(name = "USER_ID")
    private Long userId;


    @ManyToOne
    @JoinColumn(name = "KNOWLEDGE_BASE_ID", insertable=false, updatable=false)
    private KnowledgeBase knowledgeBase;

    @Id
    @ManyToOne
    @JoinColumn(name = "USER_ID", insertable=false, updatable=false)
    private User user;

    @Column(name = "GRANT")
    @Enumerated(EnumType.STRING)
    private Grant grant;

    public enum Grant {
        READ,
        WRITE
    }

    public Long getKnowledgeBaseId() {
        return knowledgeBaseId;
    }

    public void setKnowledgeBaseId(Long knowledgeBaseId) {
        this.knowledgeBaseId = knowledgeBaseId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public KnowledgeBase getKnowledgeBase() {
        return knowledgeBase;
    }

    public void setKnowledgeBase(KnowledgeBase knowledgeBase) {
        this.knowledgeBase = knowledgeBase;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Grant getGrant() {
        return grant;
    }

    public void setGrant(Grant grant) {
        this.grant = grant;
    }
}