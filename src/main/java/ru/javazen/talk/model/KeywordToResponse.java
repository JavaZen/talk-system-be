package ru.javazen.talk.model;

import javax.persistence.*;

@Entity
@Table(name = "KEYWORD_TO_RESPONSE_RELATIONS")
public class KeywordToResponse {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "RESPONSE_ID")
    private Response response;

    @ManyToOne
    @JoinColumn(name = "KEYWORD_ID")
    private Keyword keyword;

    @Column(name = "TYPE_OF_RELATION")
    private String typeOfRelation;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public Keyword getKeyword() {
        return keyword;
    }

    public void setKeyword(Keyword keyword) {
        this.keyword = keyword;
    }

    public String getTypeOfRelation() {
        return typeOfRelation;
    }

    public void setTypeOfRelation(String typeOfRelation) {
        this.typeOfRelation = typeOfRelation;
    }
}
