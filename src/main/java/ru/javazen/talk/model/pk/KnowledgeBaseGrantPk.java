package ru.javazen.talk.model.pk;

import javax.persistence.Column;
import java.io.Serializable;

/**
 * Created by Andrew on 04.06.2017.
 */
public class KnowledgeBaseGrantPk implements Serializable {

    /*@Column(name = "USER_ID")*/
    private Long userId;

    /*@Column(name = "KNOWLEDGE_BASE_ID")*/
    private Long knowledgeBaseId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getKnowledgeBaseId() {
        return knowledgeBaseId;
    }

    public void setKnowledgeBaseId(Long knowledgeBaseId) {
        this.knowledgeBaseId = knowledgeBaseId;
    }
}
