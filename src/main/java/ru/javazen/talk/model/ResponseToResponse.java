package ru.javazen.talk.model;

import javax.persistence.*;

@Entity
@Table(name = "RESPONSE_TO_RESPONSE_RELATIONS")
public class ResponseToResponse {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "RESPONSE_FROM_ID")
    private Response responseFrom;

    @ManyToOne
    @JoinColumn(name = "RESPONSE_TO_ID")
    private Response responseTo;

    @Column(name = "TYPE_OF_RELATION")
    @Enumerated(EnumType.STRING)
    private TypeOfRelation typeOfRelation;

    public enum TypeOfRelation {
        KEEP_CONTEXT("Сохранять текущий контекст"),
        USE_PARENT_CONTEXT("Использовать родительский контекст"),
        USE_ROOT_CONTEXT("Использовать корневой контекст");

        private String value;

        TypeOfRelation(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Response getResponseFrom() {
        return responseFrom;
    }

    public void setResponseFrom(Response responseFrom) {
        this.responseFrom = responseFrom;
    }

    public Response getResponseTo() {
        return responseTo;
    }

    public void setResponseTo(Response responseTo) {
        this.responseTo = responseTo;
    }

    public TypeOfRelation getTypeOfRelation() {
        return typeOfRelation;
    }

    public void setTypeOfRelation(TypeOfRelation typeOfRelation) {
        this.typeOfRelation = typeOfRelation;
    }
}
