package ru.javazen.talk.model;

import javax.persistence.*;

@Entity
@Table(name = "KEYWORD_TO_KEYWORD_RELATIONS",
        indexes = {
                @Index(columnList = "KEYWORD_FROM_ID", name = "K2K_KEYWORD_FROM_ID_IX"),
                @Index(columnList = "KEYWORD_TO_ID", name = "K2K_KEYWORD_TO_ID_IX")})
public class KeywordToKeyword {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "KEYWORD_FROM_ID")
    private Keyword keywordFrom;

    @ManyToOne
    @JoinColumn(name = "KEYWORD_TO_ID")
    private Keyword keywordTo;

    @Column(name = "TYPE_OF_RELATION")
    @Enumerated(EnumType.STRING)
    private TypeOfRelation typeOfRelation;


    public enum TypeOfRelation {
        SYNONYM("Синоним"),
        OTHER("Другое");

        private String value;

        TypeOfRelation(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Keyword getKeywordFrom() {
        return keywordFrom;
    }

    public void setKeywordFrom(Keyword keywordFrom) {
        this.keywordFrom = keywordFrom;
    }

    public Keyword getKeywordTo() {
        return keywordTo;
    }

    public void setKeywordTo(Keyword keywordTo) {
        this.keywordTo = keywordTo;
    }

    public TypeOfRelation getTypeOfRelation() {
        return typeOfRelation;
    }

    public void setTypeOfRelation(TypeOfRelation typeOfRelation) {
        this.typeOfRelation = typeOfRelation;
    }
}
