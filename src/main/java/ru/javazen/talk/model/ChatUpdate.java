package ru.javazen.talk.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "CHAT_UPDATES")
public class ChatUpdate {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "CHAT_ID")
    private Chat chat;

    @ManyToOne
    @JoinColumn(name = "USER_ID")
    private User user;

    @Column(name = "MESSAGE")
    private String message;

    @Column(name = "SENT_WHEN")
    private Date sentWhen;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Chat getChat() {
        return chat;
    }

    public void setChat(Chat chat) {
        this.chat = chat;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getSentWhen() {
        return sentWhen;
    }

    public void setSentWhen(Date sentWhen) {
        this.sentWhen = sentWhen;
    }
}
