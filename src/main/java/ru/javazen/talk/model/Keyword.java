package ru.javazen.talk.model;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "KEYWORDS",
        indexes = {
                @Index(columnList = "ID", name = "K_ID_IX"),
                @Index(columnList = "KNOWLEDGE_BASE_ID", name = "K_KNOWLEDGE_BASE_ID_IX")
        })
public class Keyword {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "TEXT")
    private String text;

    @OneToOne
    @JoinColumn(name = "KNOWLEDGE_BASE_ID")
    private KnowledgeBase knowledgeBase;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(
            name="KEYWORD_FLAGS",
            joinColumns=@JoinColumn(name="KEYWORD_ID")
    )
    @Column(name="FLAGS")
    private Set<Flag> flags;

    @Column(name = "IS_ROOT")
    private Boolean isRoot;

    @Embeddable
    public static class Flag {
        @Column(name = "NAME")
        String name;

        @Column(name = "VALUE")
        String value;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public KnowledgeBase getKnowledgeBase() {
        return knowledgeBase;
    }

    public void setKnowledgeBase(KnowledgeBase knowledgeBase) {
        this.knowledgeBase = knowledgeBase;
    }

    public Set<Flag> getFlags() {
        return flags;
    }

    public void setFlags(Set<Flag> flags) {
        this.flags = flags;
    }

    public Boolean getRoot() {
        return isRoot;
    }

    public void setRoot(Boolean root) {
        isRoot = root;
    }
}
