package ru.javazen.talk.repository;

import org.springframework.data.repository.CrudRepository;
import ru.javazen.talk.model.ChatParticipant;
import ru.javazen.talk.model.Group;

import java.util.List;

public interface ChatParticipantRepository extends CrudRepository<ChatParticipant, Long> {

    List<ChatParticipant> findChatParticipantsByChatId(Long chatId);
}
