package ru.javazen.talk.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ru.javazen.talk.model.ResponseToResponse;

import java.util.List;

public interface ResponseToResponseRepository extends CrudRepository<ResponseToResponse, Long> {

    @Query("select r2r from ResponseToResponse r2r where r2r.responseFrom.id = ?1")
    List<ResponseToResponse> getRelationsByParentId(Long r2rId);

    @Query("select r2r from ResponseToResponse r2r where r2r.responseFrom.knowledgeBase.id = ?1")
    List<ResponseToResponse> getRelationsByKnowledgeBaseId(Long kbId);

    @Query("select r2r from ResponseToResponse r2r where r2r.responseTo.id = ?1")
    ResponseToResponse findByChildResponseId(Long childId);

    @Modifying
    @Query("delete from ResponseToResponse r2r where r2r.responseFrom.id = ?1 or r2r.responseTo.id = ?1")
    void deleteByResponseId(Long responseId);
}
