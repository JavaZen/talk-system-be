package ru.javazen.talk.repository;

import org.springframework.data.repository.CrudRepository;
import ru.javazen.talk.model.Response;

import java.util.List;

public interface ResponseRepository  extends CrudRepository<Response, Long> {

    List<Response> getResponsesByKnowledgeBaseId(Long knowledgeBaseId);

    List<Response> getResponsesByKnowledgeBaseIdAndIsRootTrue(Long knowledgeBaseId);
}
