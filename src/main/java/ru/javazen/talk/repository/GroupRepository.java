package ru.javazen.talk.repository;

import org.springframework.data.repository.CrudRepository;
import ru.javazen.talk.model.Group;

public interface GroupRepository extends CrudRepository<Group, Long> {

    Group findByName(String name);
}
