package ru.javazen.talk.repository;

import org.springframework.data.repository.CrudRepository;
import ru.javazen.talk.model.Keyword;

import java.util.List;

public interface KeywordRepository  extends CrudRepository<Keyword, Long> {

    List<Keyword> getKeywordsByKnowledgeBaseId(Long knowledgeBaseId);
}