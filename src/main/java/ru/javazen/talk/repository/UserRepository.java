package ru.javazen.talk.repository;

import org.springframework.data.repository.CrudRepository;
import ru.javazen.talk.model.User;

import java.util.List;

public interface UserRepository extends CrudRepository<User, Long> {

    User findByUsernameIgnoreCase(String username);

    List<User> findAllByGroupId(Long groupId);
}
