package ru.javazen.talk.repository;

import org.springframework.data.repository.CrudRepository;
import ru.javazen.talk.model.KnowledgeBase;
import ru.javazen.talk.model.KnowledgeBaseGrant;

/**
 * Created by Andrew on 04.06.2017.
 */
public interface KnowledgeBaseGrantRepository extends CrudRepository<KnowledgeBaseGrant, Long> {

    KnowledgeBaseGrant findByKnowledgeBaseIdAndUserId(Long knowledgeBaseId, Long userId);
}
