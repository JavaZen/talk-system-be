package ru.javazen.talk.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ru.javazen.talk.model.Chat;
import ru.javazen.talk.model.Group;

import java.util.List;


public interface ChatRepository extends CrudRepository<Chat, Long> {

    @Query("select c from Chat c, ChatParticipant p1, ChatParticipant p2 " +
            "where p1.user.id = ?1 and p2.user.id = ?2 and p1.chat.id = c.id and p2.chat.id = c.id")
    List<Chat> findChatByParticipants(Long firstUserId, Long secondUserId);

    @Query("select c from Chat c, ChatParticipant p " +
            "where p.user.id = ?1 and p.chat.id = c.id")
    List<Chat> findChatByParticipant(Long userId);
}
