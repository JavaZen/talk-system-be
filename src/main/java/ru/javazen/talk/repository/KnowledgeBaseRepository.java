package ru.javazen.talk.repository;

import org.springframework.data.repository.CrudRepository;
import ru.javazen.talk.model.KnowledgeBase;

public interface KnowledgeBaseRepository extends CrudRepository<KnowledgeBase, Long> {

    KnowledgeBase findKnowledgeBaseByUserId(Long userId);
}
