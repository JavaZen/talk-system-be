package ru.javazen.talk.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ru.javazen.talk.model.Keyword;
import ru.javazen.talk.model.KeywordToResponse;

import java.util.List;

public interface KeywordToResponseRepository extends CrudRepository<KeywordToResponse, Long> {

    @Query("select ktr from KeywordToResponse ktr where ktr.response.knowledgeBase.id = ?1")
    List<KeywordToResponse> findAllByKnowledgeBaseId(Long knowledgeBaseId);

    @Query("select k from Keyword k " +
            "inner join KeywordToResponse ktr " +
            "on ktr.response.id = ?1 and ktr.keyword.id = k.id")
    List<Keyword> getKeywordsByResponseId(Long parentId);

    @Modifying
    @Query("delete from KeywordToResponse ktr where ktr.response.id = ?1")
    void removeKeywordRelationsByResponseId(Long responseId);

    @Modifying
    @Query("delete from KeywordToResponse ktr where ktr.keyword.id = ?1")
    void removeKeywordRelationsByKeywordId(Long keywordId);

    @Modifying
    @Query("delete from KeywordToResponse ktr where ktr.keyword.id = ?1 and ktr.response.id = ?2")
    void removeKeywordRelationsByKeywordAndResponseIds(Long keywordId, Long responseId);
}
