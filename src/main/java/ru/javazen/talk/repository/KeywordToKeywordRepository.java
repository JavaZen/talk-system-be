package ru.javazen.talk.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ru.javazen.talk.model.Keyword;
import ru.javazen.talk.model.KeywordToKeyword;

import java.util.List;

public interface KeywordToKeywordRepository  extends CrudRepository<KeywordToKeyword, Long> {

    @Query("select k from Keyword k " +
            "inner join KeywordToKeyword ktk " +
            "on ktk.keywordFrom.id = ?1 and ktk.keywordTo.id = k.id")
    List<Keyword> getKeywordsByParentId(Long parentId);

    @Query("select ktk from KeywordToKeyword ktk where ktk.keywordFrom.id = ?1")
    List<KeywordToKeyword> getRelationsByParentId(Long parentId);

    @Modifying
    @Query("delete from KeywordToKeyword ktk where ktk.keywordFrom.id = ?1 or ktk.keywordTo.id = ?1")
    void removeAnyByKeywordId(Long keywordId);

}
