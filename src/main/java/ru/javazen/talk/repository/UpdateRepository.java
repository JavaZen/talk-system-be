package ru.javazen.talk.repository;

import org.springframework.data.repository.CrudRepository;
import ru.javazen.talk.model.ChatUpdate;

public interface UpdateRepository extends CrudRepository<ChatUpdate, Long> {
}
