package ru.javazen.talk.repository;

import org.springframework.data.repository.CrudRepository;
import ru.javazen.talk.model.Role;

public interface RoleRepository extends CrudRepository<Role, Long> {

    Role findByAuthority(String authority);
}