package ru.javazen.talk.repository;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import ru.javazen.talk.model.ChatUpdate;

import java.util.List;

/**
 * Created by Andrew on 20.04.2017.
 */
public interface ChatUpdateRepository extends CrudRepository<ChatUpdate, Long> {

    @Query("select cu from ChatUpdate cu where cu.chat.id = ?1 order by cu.id desc")
    List<ChatUpdate> getLastUpdates(Long chatId, Pageable pageable);

    @Query("select cu from ChatUpdate cu where cu.chat.id = ?1 and cu.id < ?2 order by cu.id asc")
    List<ChatUpdate> getUpdatesWithOffset(Long chatId, Long offsetUpdateId, Pageable pageable);

    @Query("select cu from ChatUpdate cu where cu.chat.id = ?1 and cu.id > ?2 order by cu.id asc")
    List<ChatUpdate> getUpdatesFromMessage(Long chatId, Long fromUpdateId);
}
