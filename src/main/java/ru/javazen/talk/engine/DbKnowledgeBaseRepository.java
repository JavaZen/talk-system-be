package ru.javazen.talk.engine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import ru.javazen.talk.knowledgebase.Keyword;
import ru.javazen.talk.knowledgebase.KeywordToKeyword;
import ru.javazen.talk.knowledgebase.KeywordToResponse;
import ru.javazen.talk.knowledgebase.Response;
import ru.javazen.talk.knowledgebase.ResponseToResponse;
import ru.javazen.talk.knowledgebase.repository.KnowledgeBaseRepository;
import ru.javazen.talk.repository.*;

import java.util.*;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Provide access to KnowledgeBase from database for TalkEngine
 * TODO implement it!!!
 * todo cache....
 */
public class DbKnowledgeBaseRepository implements KnowledgeBaseRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(DbKnowledgeBaseRepository.class);

    private Long knowledgeBaseId;

    public void setKnowledgeBaseId(Long knowledgeBaseId) {
        LOGGER.trace("Created new repository for KB with {} id", knowledgeBaseId);
        this.knowledgeBaseId = knowledgeBaseId;
    }

    @Autowired
    private KeywordRepository keywordRepository;

    @Autowired
    private ResponseRepository responseRepository;

    @Autowired
    private KeywordToKeywordRepository keywordToKeywordRepository;

    @Autowired
    private KeywordToResponseRepository keywordToResponseRepository;

    @Autowired
    private ResponseToResponseRepository responseToResponseRepository;

    // returns all keywords from tree as plain list of keywords
    @Override
    public Collection<Keyword> getAllKeywords() {
        List<ru.javazen.talk.model.Keyword> dbKeywords  = keywordRepository.getKeywordsByKnowledgeBaseId(knowledgeBaseId);
        List<ru.javazen.talk.model.Keyword> childKeywords = findChildKeywords(dbKeywords);
        if (childKeywords != null) {
            dbKeywords.addAll(childKeywords);
        }
        List<Keyword> keywords = new ArrayList<>();
        for (ru.javazen.talk.model.Keyword dbKeyword : dbKeywords) {
            Keyword keyword = new Keyword();
            keyword.setId(dbKeyword.getId());
            keyword.setText(dbKeyword.getText());

            if (dbKeyword.getFlags() != null) {
                Set<Keyword.Flag> flags = new HashSet<>();
                for (ru.javazen.talk.model.Keyword.Flag flag : dbKeyword.getFlags()) {
                    flags.add(Keyword.Flag.valueOf(flag.getName().toUpperCase()));
                }
                keyword.setFlags(flags);
            }

            keywords.add(keyword);
        }
        LOGGER.trace("Returned keywords: {}", keywords);
        return keywords;
    }

    private List<ru.javazen.talk.model.Keyword> findChildKeywords(List<ru.javazen.talk.model.Keyword> keywords) {
        List<ru.javazen.talk.model.Keyword> result = new ArrayList<>();

        for (ru.javazen.talk.model.Keyword keyword : keywords) {
            List<ru.javazen.talk.model.Keyword> foundedKeywords;
            foundedKeywords = keywordToKeywordRepository.getKeywordsByParentId(keyword.getId());
            if (foundedKeywords != null && !foundedKeywords.isEmpty()) {

                List<ru.javazen.talk.model.Keyword> childKeywords = findChildKeywords(foundedKeywords);
                if (childKeywords != null) {
                    foundedKeywords.addAll(childKeywords);
                }
                result.addAll(foundedKeywords);
            }
        }
        return result;
    }

    @Override
    public Collection<Response> getAllResponse() {
        List<ru.javazen.talk.model.Response> dbResponses = responseRepository.getResponsesByKnowledgeBaseId(knowledgeBaseId);
        List<Response> responses = new ArrayList<>();
        for (ru.javazen.talk.model.Response dbResponse : dbResponses) {
            Response response = new Response();
            response.setId(dbResponse.getId());
            response.setText(dbResponse.getText());
            response.setType(Response.Type.valueOf(dbResponse.getType().name()));
            response.setContextBehavior(
                    Response.ContextBehavior.valueOf(dbResponse.getContextBehavior().name()));

            if (dbResponse.getFlags() != null) {
                Set<Response.Flag> flags = new HashSet<>();
                dbResponse.getFlags().forEach(f -> {
                    flags.add(Response.Flag.valueOf(f.getName()));
                });
                response.setFlags(flags);
            }
            responses.add(response);
        }
        LOGGER.trace("Returned responses: {}", responses);
        return responses;
    }

    @Override
    public Collection<KeywordToKeyword> getAllKeywordToKeywords() {
        List<ru.javazen.talk.model.Keyword> dbKeywords  = keywordRepository.getKeywordsByKnowledgeBaseId(knowledgeBaseId);

        List<ru.javazen.talk.model.KeywordToKeyword> toKeywords = getKeywordToKeywordAsList(dbKeywords);

        List<KeywordToKeyword> result = new ArrayList<>();

        toKeywords.forEach(k2k -> {
            KeywordToKeyword keywordToKeyword = new KeywordToKeyword();
            keywordToKeyword.setId(k2k.getId());
            if (k2k.getTypeOfRelation() != null) {
                keywordToKeyword.setTypeOfRelation(KeywordToKeyword.Type.valueOf(k2k.getTypeOfRelation().name()));
            }

            Keyword keywordFrom = new Keyword();
            keywordFrom.setId(k2k.getKeywordFrom().getId());
            keywordFrom.setText(k2k.getKeywordFrom().getText());
            if (k2k.getKeywordFrom().getFlags() != null) {
                Set<Keyword.Flag> flags = new HashSet<>();
                for (ru.javazen.talk.model.Keyword.Flag flag : k2k.getKeywordFrom().getFlags()) {
                    flags.add(Keyword.Flag.valueOf(flag.getName().toUpperCase()));
                }
                keywordFrom.setFlags(flags);
            }

            keywordToKeyword.setRelatedFrom(keywordFrom);

            Keyword keywordTo = new Keyword();
            keywordTo.setId(k2k.getKeywordTo().getId());
            keywordTo.setText(k2k.getKeywordTo().getText());
            if (k2k.getKeywordTo().getFlags() != null) {
                Set<Keyword.Flag> flags = new HashSet<>();
                for (ru.javazen.talk.model.Keyword.Flag flag : k2k.getKeywordTo().getFlags()) {
                    flags.add(Keyword.Flag.valueOf(flag.getName().toUpperCase()));
                }
                keywordTo.setFlags(flags);
            }

            keywordToKeyword.setRelatedTo(keywordTo);

            result.add(keywordToKeyword);
        });

        LOGGER.trace("Returned KeywordToKeywords: {}", result);
        return result;
    }

    /**
     * Gets a list of all keyword. Takes for each keyword related entities type of KeywordToKeyword. Put it to return result.
     * Gets from each entity keyword (keyword_to) and save it to collection. Calls for the list self. Save obtained result to return result.
     *
     * @param dbKeywords root DB keywords list
     * @return all entities type of KeywordToKeyword of knowledge base as plain list
     */
    private List<ru.javazen.talk.model.KeywordToKeyword> getKeywordToKeywordAsList(List<ru.javazen.talk.model.Keyword> dbKeywords) {

        List<ru.javazen.talk.model.KeywordToKeyword> result = new ArrayList<>();
        for (ru.javazen.talk.model.Keyword keyword : dbKeywords) {
            List<ru.javazen.talk.model.KeywordToKeyword> founded = keywordToKeywordRepository.getRelationsByParentId(keyword.getId());
            if (!founded.isEmpty()) {
                result.addAll(founded);

                List<ru.javazen.talk.model.Keyword> keywords = founded.stream().map(ru.javazen.talk.model.KeywordToKeyword::getKeywordTo).collect(Collectors.toList());
                List<ru.javazen.talk.model.KeywordToKeyword> recRes = getKeywordToKeywordAsList(keywords);
                if (recRes != null && !recRes.isEmpty()) {
                    result.addAll(recRes);
                }
            }
        }
        return result;
    }

    @Override
    public Collection<KeywordToResponse> getAllKeywordToResponses() {

        List<ru.javazen.talk.model.KeywordToResponse> keywordToResponses = keywordToResponseRepository.findAllByKnowledgeBaseId(knowledgeBaseId);
        List<KeywordToResponse> result = new ArrayList<>();

        keywordToResponses.forEach(k2r -> {
            KeywordToResponse keywordToResponse = new KeywordToResponse();
            keywordToResponse.setId(k2r.getId());
            // TODO keywordToResponse.setTypeOfRelation(KeywordToResponse.Type.valueOf(k2r.getTypeOfRelation().toUpperCase()));

            Keyword keyword = new Keyword();
            keyword.setId(k2r.getKeyword().getId());
            keyword.setText(k2r.getKeyword().getText());

            if (k2r.getKeyword().getFlags() != null) {
                Set<Keyword.Flag> flags = new HashSet<>();
                for (ru.javazen.talk.model.Keyword.Flag flag : k2r.getKeyword().getFlags()) {
                    flags.add(Keyword.Flag.valueOf(flag.getName().toUpperCase()));
                }
                keyword.setFlags(flags);
            }
            keywordToResponse.setRelatedFrom(keyword);

            Response response = new Response();
            response.setId(k2r.getResponse().getId());
            response.setText(k2r.getResponse().getText());
            response.setType(Response.Type.valueOf(k2r.getResponse().getType().name()));
            response.setContextBehavior(
                    Response.ContextBehavior.valueOf(k2r.getResponse().getContextBehavior().name()));


            if (k2r.getResponse().getFlags() != null) {
                Set<Response.Flag> flags = new HashSet<>();
                k2r.getResponse().getFlags().forEach(f -> {
                    flags.add(Response.Flag.valueOf(f.getName()));
                });
                response.setFlags(flags);
            }
            keywordToResponse.setRelatedTo(response);

            result.add(keywordToResponse);
        });

        LOGGER.trace("Returned KeywordToResponses {}", result);
        return result;
    }



    @Override
    public Collection<ResponseToResponse> getAllResponseToResponses() {

        Iterable<ru.javazen.talk.model.ResponseToResponse> responseToResponses
                =responseToResponseRepository.getRelationsByKnowledgeBaseId(knowledgeBaseId);  //TODO

        List<ResponseToResponse> results = new ArrayList<>();

        responseToResponses.forEach(r2r -> {
            ResponseToResponse result = new ResponseToResponse();
            Response responseFrom = new Response();
            responseFrom.setId(r2r.getResponseFrom().getId());
            responseFrom.setText(r2r.getResponseFrom().getText());
            responseFrom.setType(Response.Type.valueOf(r2r.getResponseFrom().getType().name()));
            responseFrom.setContextBehavior(
                    Response.ContextBehavior.valueOf(r2r.getResponseFrom().getContextBehavior().name()));

            if (r2r.getResponseFrom().getFlags() != null) {
                Set<Response.Flag> flags = new HashSet<>();
                r2r.getResponseFrom().getFlags().forEach(f -> {
                    flags.add(Response.Flag.valueOf(f.getName()));
                });
                responseFrom.setFlags(flags);
            }

            Response responseTo = new Response();
            responseTo.setId(r2r.getResponseTo().getId());
            responseTo.setText(r2r.getResponseTo().getText());
            responseTo.setType(Response.Type.valueOf(r2r.getResponseTo().getType().name()));
            responseTo.setContextBehavior(
                    Response.ContextBehavior.valueOf(r2r.getResponseTo().getContextBehavior().name()));

            if (r2r.getResponseTo().getFlags() != null) {
                Set<Response.Flag> flags = new HashSet<>();
                r2r.getResponseTo().getFlags().forEach(f -> {
                    flags.add(Response.Flag.valueOf(f.getName()));
                });
                responseTo.setFlags(flags);
            }

            result.setId(r2r.getId());
            result.setRelatedFrom(responseFrom);
            result.setRelatedTo(responseTo);
            /*if (r2r.getTypeOfRelation() != null) {
                result.setTypeOfRelation(ResponseToResponse.Type.valueOf(
                        r2r.getTypeOfRelation().name()));
            }*/
            results.add(result);
        });

        return results;
    }

    @Override
    public Keyword saveKeyword(Keyword keyword) {
        return null;
    }

    @Override
    public Response saveResponse(Response response) {
        return null;
    }

    @Override
    public KeywordToKeyword saveKeywordToKeyword(KeywordToKeyword keywordToKeyword) {
        return null;
    }

    @Override
    public KeywordToResponse saveKeywordToResponse(KeywordToResponse keywordToResponse) {
        return null;
    }

    @Override
    public ResponseToResponse saveResponseToResponse(ResponseToResponse responseToResponse) {
        return null;
    }

    @Override
    public boolean removeKeyword(Keyword keyword) {
        return false;
    }

    @Override
    public boolean removeResponse(Response response) {
        return false;
    }

    @Override
    public boolean removeKeywordToKeyword(KeywordToKeyword keywordToKeyword) {
        return false;
    }

    @Override
    public boolean removeKeywordToResponse(KeywordToResponse keywordToResponse) {
        return false;
    }

    @Override
    public boolean removeResponseToResponse(ResponseToResponse responseToResponse) {
        return false;
    }
}
