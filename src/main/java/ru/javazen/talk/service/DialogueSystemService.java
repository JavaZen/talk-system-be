package ru.javazen.talk.service;


import ru.javazen.talk.model.ChatUpdate;

/**
 * Created by Andrew on 25.04.2017.
 */
public interface DialogueSystemService {

    void sendUpdate(ChatUpdate update);

    void resetContext(Long chatId);
}
