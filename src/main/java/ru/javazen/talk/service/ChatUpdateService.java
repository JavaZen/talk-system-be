package ru.javazen.talk.service;

import ru.javazen.talk.rest.transport.response.ChatExplanationUpdateResponse;
import ru.javazen.talk.model.ChatUpdate;

import java.util.List;

public interface ChatUpdateService {

    void sendUpdate(ChatUpdate update);

    List<ChatUpdate> getLastUpdates(Long chatId, int lastUpdatesCount);

    List<ChatUpdate> getUpdatesWithOffset(Long chatId, int updatesCount, Long offsetUpdateId);

    List<ChatUpdate> getUpdatesFromMessage(Long chatId, Long fromUpdateId);

    void sendExplanation(ChatExplanationUpdateResponse update);

    void resetContext(Long chatId);
}
