package ru.javazen.talk.service;

import org.springframework.web.context.request.async.DeferredResult;
import ru.javazen.talk.rest.transport.response.LongPollingUpdate;

import java.util.List;

/**
 * Created by Andrew on 22.04.2017.
 */
public interface LongPollingService {

    void sendUpdate(LongPollingUpdate update);

    void sendUpdate(List<LongPollingUpdate> updates);

    void registerForUpdate(DeferredResult<List<LongPollingUpdate>> results, Long fromId);

    void registerForUpdate(DeferredResult<List<LongPollingUpdate>> results, String type, Long fromId);
}
