package ru.javazen.talk.service;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import ru.javazen.talk.model.User;

import java.util.Collection;

/**
 * Created by Andrew on 01.05.2017.
 */
public interface UserService extends UserDetailsService {

    @Override
    User loadUserByUsername(String s) throws UsernameNotFoundException;

    User saveUser(User user);

    Iterable<User> findAll();

    User signupUser(User user);

    User registerKbUser(User user);

    User getCurrentUser();

    boolean hasRole(String role);
}
