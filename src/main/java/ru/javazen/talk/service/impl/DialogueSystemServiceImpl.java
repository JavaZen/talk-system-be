package ru.javazen.talk.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import ru.javazen.talk.analysis.grapheme.GraphemeAnalyzer;
import ru.javazen.talk.analysis.morphology.MorphologyAnalyzer;
import ru.javazen.talk.analysis.util.KeywordComparator;
import ru.javazen.talk.dialog.Dialog;
import ru.javazen.talk.dialog.ElizaDialog;
import ru.javazen.talk.dialog.Reply;
import ru.javazen.talk.rest.transport.response.ChatExplanationUpdateResponse;
import ru.javazen.talk.event.DialogEvent;
import ru.javazen.talk.event.DialogExplanationEvent;
import ru.javazen.talk.event.EventSubscriber;
import ru.javazen.talk.event.ReplyEvent;
import ru.javazen.talk.explanation.data.Base;
import ru.javazen.talk.model.ChatParticipant;
import ru.javazen.talk.model.ChatUpdate;
import ru.javazen.talk.model.KnowledgeBase;
import ru.javazen.talk.repository.ChatParticipantRepository;
import ru.javazen.talk.repository.ChatRepository;
import ru.javazen.talk.repository.KnowledgeBaseRepository;
import ru.javazen.talk.repository.UserRepository;
import ru.javazen.talk.service.ChatUpdateService;
import ru.javazen.talk.service.DialogueSystemService;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Andrew on 25.04.2017.
 */
public abstract class DialogueSystemServiceImpl implements DialogueSystemService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DialogueSystemServiceImpl.class);

    private long dialogIds = 1;

    @Autowired
    private ChatUpdateService chatUpdateService;

    @Autowired
    private ChatParticipantRepository chatParticipantRepository;

    @Autowired
    private KnowledgeBaseRepository knowledgeBaseRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ChatRepository chatRepository;

    @Autowired
    private MorphologyAnalyzer morphologyAnalyzer;

    @Autowired
    private GraphemeAnalyzer graphemeAnalyzer;

    @Autowired
    private KeywordComparator keywordComparator;

    List<Long> sentUpdates = new ArrayList<>();

    //chat_id --> kb_id --> dialog
    private Map<Long, Map<Long, Dialog>> dialoguesByChatId = new HashMap<>();


    @Async
    public void sendUpdate(ChatUpdate update) {
        try { //TODO
            LOGGER.trace("Obtained update with ID = ", update.getId());

            List<ChatParticipant> chatParticipants = chatParticipantRepository.findChatParticipantsByChatId(
                    update.getChat().getId());

            if (LOGGER.isTraceEnabled()) {
                LOGGER.trace("Chat participant ids: {}", chatParticipants.stream().map(ChatParticipant::getId).collect(Collectors.toList()));
            }

            for (ChatParticipant participant : chatParticipants) {
                KnowledgeBase knowledgeBase = knowledgeBaseRepository.findKnowledgeBaseByUserId(
                        participant.getUser().getId());

                // for searching in participants KB user
                if (knowledgeBase != null) {
                    if (update.getUser().getId().equals(knowledgeBase.getUser().getId())) {
                        return; // for exclude from chat update the dialogue system that sent the update
                    }
                    LOGGER.trace("Update will be sent to dialogue system with KB id = {}. KB user id = {}",
                            knowledgeBase.getId(), participant.getUser().getId());
                    // here... we need notify dialogue system about chat update...
                    Long chatId = update.getChat().getId();
                    Long kbId = knowledgeBase.getId();

                    Dialog dialog = assureAndGetDialog(chatId, kbId);

                    Reply reply = new Reply();
                    reply.setId(new Random().nextLong());
                    reply.setText(update.getMessage());
                    reply.setType(Reply.Type.QUESTION_FROM_USER);

                    dialog.sendReply(reply);
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error during send reply", e);
        }
    }

    private Dialog assureAndGetDialog(Long chatId, Long kbId) {

        Map <Long, Dialog> dialogMap = dialoguesByChatId.get(chatId);
        if (dialogMap == null) {
            dialogMap = new HashMap<>();
            dialoguesByChatId.put(chatId, dialogMap);
        }

        Dialog dialog = dialogMap.get(kbId);
        if (dialog == null) {
            LOGGER.trace("Dialogue system with KB id = {} for " +
                    "chat id = {} already not exist, because will be created", kbId, chatId);
            KnowledgeBase base = knowledgeBaseRepository.findOne(kbId);

            ElizaDialog elizaDialog = new ElizaDialog();
            elizaDialog.setGraphemeAnalyzer(graphemeAnalyzer);
            elizaDialog.setMorphologyAnalyzer(morphologyAnalyzer);
            elizaDialog.setKeywordComparator(keywordComparator);

            dialog = elizaDialog;
            dialog.setId(dialogIds++);

            ru.javazen.talk.knowledgebase.repository.KnowledgeBaseRepository dbKnowledgeBaseRepository
                    = getKnowledgeBaseRepository(kbId);

            dialog.setKnowledgeBaseRepository(dbKnowledgeBaseRepository);
            dialog.subscribeToEvent(new DialogSubscriber(chatId, base.getUser().getId()), ReplyEvent.class);
            dialog.subscribeToEvent(new DialogExplanationSubscriber(chatId, base.getUser().getId()), ReplyEvent.class);
            dialogMap.put(kbId, dialog);
        }
        LOGGER.trace("Dialogue system with KB id = {} for chat with id = {} has returned", kbId, chatId);
        return dialog;
    }

    protected abstract ru.javazen.talk.knowledgebase.repository.KnowledgeBaseRepository getKnowledgeBaseRepository(Long kbId);

    private class DialogSubscriber implements EventSubscriber<ReplyEvent> {

        private Long chatId;

        private Long userId;

        public DialogSubscriber(Long chatId, Long userId) {
            this.chatId = chatId;
            this.userId = userId;
        }

        @Override
        public Class<ReplyEvent> getType() {
            return ReplyEvent.class;
        }

        @Override
        public void handleDialogEvent(DialogEvent dialogEvent) {
            LOGGER.trace("Handle dialog event for chat id = {}, user id = {}", chatId, userId);
            Reply event = (Reply) dialogEvent.getEntity();

            ChatUpdate update = new ChatUpdate();
            update.setUser(userRepository.findOne(userId));
            update.setChat(chatRepository.findOne(chatId));
            update.setMessage(event.getText());
            update.setSentWhen(new Date());

            chatUpdateService.sendUpdate(update);
        }
    }

    private class DialogExplanationSubscriber implements EventSubscriber<DialogExplanationEvent> {

        private Long chatId;

        private Long userId;

        public DialogExplanationSubscriber(Long chatId, Long userId) {
            this.chatId = chatId;
            this.userId = userId;
        }

        @Override
        public Class<DialogExplanationEvent> getType() {
            return DialogExplanationEvent.class;
        }

        @Override
        public void handleDialogEvent(DialogEvent dialogEvent) {
            LOGGER.trace("Handle dialog event for chat id = {}, user id = {}", chatId, userId);
            Base event = (Base) dialogEvent.getEntity();

            ChatExplanationUpdateResponse update = new ChatExplanationUpdateResponse();
            update.setBase(event);
            update.setChatId(chatId);

            chatUpdateService.sendExplanation(update);

            LOGGER.trace("Obtain explanation: {}", event);
        }
    }

    @Override
    public void resetContext(Long chatId) {
        Map <Long, Dialog> dialogMap = dialoguesByChatId.get(chatId);

        for (Map.Entry<Long, Dialog> longDialogEntry : dialogMap.entrySet()) {
            Dialog dialog = longDialogEntry.getValue();
            dialog.resetContext();
        }
    }
}
