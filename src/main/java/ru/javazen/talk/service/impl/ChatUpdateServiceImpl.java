package ru.javazen.talk.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.QPageRequest;
import org.springframework.stereotype.Service;
import ru.javazen.talk.rest.transport.response.ChatExplanationUpdateResponse;
import ru.javazen.talk.model.ChatUpdate;
import ru.javazen.talk.repository.ChatUpdateRepository;
import ru.javazen.talk.rest.transport.response.ChatUpdateResponse;
import ru.javazen.talk.rest.transport.response.LongPollingUpdate;
import ru.javazen.talk.service.ChatUpdateService;
import ru.javazen.talk.service.DialogueSystemService;
import ru.javazen.talk.service.LongPollingService;

import java.util.*;

@Service
public class ChatUpdateServiceImpl implements ChatUpdateService {
    private static final Logger LOGGER = LoggerFactory.getLogger(ChatUpdateServiceImpl.class);

    @Autowired
    private ChatUpdateRepository chatUpdateRepository;

    @Autowired
    private DialogueSystemService dialogueSystemService;

    @Autowired
    private LongPollingService longPollingService;

    @Override
    public void sendUpdate(ChatUpdate update) {
        LOGGER.trace("Obtain new update from chat with id = {}. User id = {}", update.getChat().getId(), update.getUser().getId());

        update = chatUpdateRepository.save(update);
        LOGGER.trace("Update has been saved with {} id", update.getId());

        dialogueSystemService.sendUpdate(update);

        LongPollingUpdate<ChatUpdateResponse> longPollingUpdate = new LongPollingUpdate<>();
        //longPollingUpdate.setId(new Date().getTime() + new Random().nextLong());
        longPollingUpdate.setType("chat");

        ChatUpdateResponse response = new ChatUpdateResponse();
        response.setId(update.getId());
        response.setUserId(update.getUser().getId());
        response.setChatId(update.getChat().getId());
        response.setMessage(update.getMessage());
        response.setSentWhen(update.getSentWhen());

        longPollingUpdate.setData(response);
        longPollingService.sendUpdate(longPollingUpdate);
        LOGGER.trace("Update sent to LongPolling service");
    }

    @Override
    public void sendExplanation(ChatExplanationUpdateResponse update) {
        LongPollingUpdate<ChatExplanationUpdateResponse> longPollingUpdate = new LongPollingUpdate<>();

        //longPollingUpdate.setId(new Date().getTime() + new Random().nextLong());
        longPollingUpdate.setType("explanation_chat_" + update.getChatId());

        longPollingUpdate.setData(update);
        longPollingService.sendUpdate(longPollingUpdate);
        LOGGER.trace("Update sent to LongPolling service");
    }

    @Override
    public List<ChatUpdate> getLastUpdates(Long chatId, int lastUpdatesCount) {
        Pageable pageable = new QPageRequest(0, lastUpdatesCount);
        return chatUpdateRepository.getLastUpdates(chatId, pageable);
    }

    @Override
    public List<ChatUpdate> getUpdatesWithOffset(Long chatId, int updatesCount, Long offsetUpdateId) {
        Pageable pageable = new QPageRequest(0, updatesCount);
        return chatUpdateRepository.getUpdatesWithOffset(chatId, offsetUpdateId, pageable);
    }

    @Override
    public List<ChatUpdate> getUpdatesFromMessage(Long chatId, Long fromUpdateId) {
        return chatUpdateRepository.getUpdatesFromMessage(chatId, fromUpdateId);
    }

    @Override
    public void resetContext(Long chatId) {
        dialogueSystemService.resetContext(chatId);

    }
}