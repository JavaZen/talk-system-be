package ru.javazen.talk.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.async.DeferredResult;
import ru.javazen.talk.rest.transport.response.LongPollingUpdate;
import ru.javazen.talk.service.LongPollingService;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by Andrew on 22.04.2017.
 */
@Service
public class LongPollingServiceImpl implements LongPollingService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LongPollingServiceImpl.class);

    private List<LongPollingUpdate> queue = new ArrayList<>();
    private volatile long ids = 0;

    private List<DeferredResult<List<LongPollingUpdate>>> deferredResults = new ArrayList<>();

    private Map<String, List<DeferredResult<List<LongPollingUpdate>>>> deferredResultsByType = new HashMap<>();

    @Override
    public void sendUpdate(LongPollingUpdate update) {
        update.setId(++ids);
        queue.add(update);
        Iterator<DeferredResult<List<LongPollingUpdate>>> iterator = deferredResults.iterator();

        while (iterator.hasNext()) {
            DeferredResult<List<LongPollingUpdate>> result  = iterator.next(); // must be called before you can call i.remove()

            LOGGER.trace("Sent update: {} ", update);
            result.setResult(Collections.singletonList(update));

            iterator.remove();
        }

        /*for (DeferredResult<List<LongPollingUpdate>> result : deferredResults) {
            LOGGER.trace("Sent update: {} ", update);
            result.setResult(Collections.singletonList(update));
        }*/

        List<DeferredResult<List<LongPollingUpdate>>> resByType = deferredResultsByType.remove(update.getType());

        if (resByType != null) {
            for (DeferredResult<List<LongPollingUpdate>> result : resByType) {
                LOGGER.trace("Sent update: {} ", update);
                result.setResult(Collections.singletonList(update));
            }
        }
    }

    @Override
    public void sendUpdate(List<LongPollingUpdate> updates) {
        updates.forEach(longPollingUpdate -> longPollingUpdate.setId(ids++));
        queue.addAll(updates);

        Iterator<DeferredResult<List<LongPollingUpdate>>> iterator = deferredResults.iterator();

        while (iterator.hasNext()) {
            DeferredResult<List<LongPollingUpdate>> result  = iterator.next(); // must be called before you can call i.remove()

            LOGGER.trace("Sent updates: {} ", updates);
            result.setResult(updates);

            iterator.remove();
        }

        /*for (DeferredResult<List<LongPollingUpdate>> result : deferredResults) {
            LOGGER.trace("Sent updates: {} ", updates);
            result.setResult(updates);
        }*/

        Map<String, List<LongPollingUpdate>> updatesByType = new HashMap<>();
        for (LongPollingUpdate update : updates) {
            if (updatesByType.containsKey(update.getType())) {
                updatesByType.get(update.getType()).add(update);
            } else {
                updatesByType.put(update.getType(), new ArrayList<LongPollingUpdate>() {{ add(update); }});
            }
        }
        for (Map.Entry<String, List<LongPollingUpdate>> entry : updatesByType.entrySet()) {
            List<DeferredResult<List<LongPollingUpdate>>> deferredResults = deferredResultsByType.remove(entry.getKey());
            if (deferredResults != null) {
                for (DeferredResult<List<LongPollingUpdate>> result : deferredResults) {
                    LOGGER.trace("Sent updates: {} ", entry.getValue());
                    result.setResult(entry.getValue());
                }
            }
        }
    }

    @Override
    public void registerForUpdate(DeferredResult<List<LongPollingUpdate>> results, Long fromId) {
        if (fromId != null) {
            List<LongPollingUpdate> updates = queue.stream()
                    .filter(longPollingUpdate -> longPollingUpdate.getId() > fromId)
                    .collect(Collectors.toList());
            if (updates != null && !updates.isEmpty()) {
                LOGGER.trace("Sent old updates from id = {}", fromId);
                LOGGER.trace("Sent updates: {} ", updates);
                results.setResult(updates);
                return;
            }
        }
        LOGGER.trace("New subscriber registered for all updates");
        deferredResults.add(results);
        results.onTimeout(() -> deferredResults.remove(results));
    }

    @Override
    public void registerForUpdate(DeferredResult<List<LongPollingUpdate>> results, String type, Long fromId) {
        if (fromId != null) {
            List<LongPollingUpdate> updates = queue.stream()
                    .filter(longPollingUpdate -> longPollingUpdate.getId() > fromId && type.equalsIgnoreCase(longPollingUpdate.getType()))
                    .collect(Collectors.toList());
            if (updates != null && !updates.isEmpty()) {
                LOGGER.trace("Sent old updates from id = {}, type = {}", fromId, type);
                LOGGER.trace("Sent updates: {} ", updates);
                results.setResult(updates);
                return;
            }
        }

        LOGGER.trace("New subscriber registered for updates of {} type", type);
        if (deferredResultsByType.containsKey(type)) {
            deferredResultsByType.get(type).add(results);
        } else {
            deferredResultsByType.put(type, new ArrayList<DeferredResult<List<LongPollingUpdate>>>() {{ add(results); }});
        }
        results.onTimeout(() -> deferredResultsByType.get(type).remove(results) );
    }

    //
    // maybe I need to implement delivery of updates by last ID in some cases?
    //
}