package ru.javazen.talk.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.javazen.talk.model.Group;
import ru.javazen.talk.model.Role;
import ru.javazen.talk.model.User;
import ru.javazen.talk.repository.GroupRepository;
import ru.javazen.talk.repository.RoleRepository;
import ru.javazen.talk.repository.UserRepository;
import ru.javazen.talk.service.UserService;

import java.text.MessageFormat;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by Andrew on 01.05.2017.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private GroupRepository groupRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public User loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsernameIgnoreCase(username);
        if (user == null) {
            throw new UsernameNotFoundException(MessageFormat.format("Can't found user with username: {0}", username));
        }
        return user;
    }

    @Override
    public User saveUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public Iterable<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User signupUser(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        Role role = roleRepository.findByAuthority("ROLE_USER"); //TODO
        user.setRoles(Collections.singleton(role));

        Group group = groupRepository.findOne(2L); //TODO
        user.setGroup(group);

        return userRepository.save(user);
    }

    @Override
    public User registerKbUser(User user) {
        user.setPassword(passwordEncoder.encode(user.getPassword()));

        Role role = roleRepository.findByAuthority("ROLE_BOT"); //TODO
        user.setRoles(Collections.singleton(role));

        Group group = groupRepository.findOne(3L); //TODO
        user.setGroup(group);

        return userRepository.save(user);
    }

    @Override
    public User getCurrentUser() {
        UserDetails userDetails = (UserDetails)SecurityContextHolder.getContext().
                getAuthentication().getPrincipal();
        User user = null;
        if (userDetails instanceof User) {
            user =  (User) userDetails;
        }
        return user;
    }

    @Override
    public boolean hasRole(String role) {
        Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>)
                SecurityContextHolder.getContext().getAuthentication().getAuthorities();

        boolean hasRole = false;
        for (GrantedAuthority authority : authorities) {
            hasRole = authority.getAuthority().equals(role);
            if (hasRole) {
                break;
            }
        }
        return hasRole;
    }
}
